#include "GetSecretLayer.h"
#include "StageBaseScene.h"

USING_NS_CC;
using namespace ui;

bool GetSecretLayer::init()
{
    if ( !LayerColor::initWithColor(Color4B(0, 0, 0, 0)) )
    {
        return false;
    }
    //下のレイヤーにタッチを貫通させない
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = [](Touch *touch,Event*event)->bool{ return true;};
    auto dip = Director::getInstance()->getEventDispatcher();
    dip->addEventListenerWithSceneGraphPriority(listener, this);
    dip->setPriority(listener, -1);
    //
    cocos2d::CallFunc *callback = CallFunc::create([this]()
                                                   {
                                                       startAction();
                                                   });
    this->runAction(Sequence::create(FadeTo::create(0.2, 160), callback, NULL));
    
    return true;
}

void GetSecretLayer::startAction()
{
    //画像読み込み
    _secretSprite = Sprite::create("Graphics/sp/getSp.png");
    _secretSprite->setAnchorPoint(Point(0.5, 0.2));
    _secretSprite->setPosition(Point(320, 570));
    this->addChild(_secretSprite, SECRET);
    
    _getSprite = Sprite::create("Graphics/sp/get.png");
    _getSprite->setAnchorPoint(Point::ANCHOR_MIDDLE_BOTTOM);
    _getSprite->setPosition(Point(320, 400));
    this->addChild(_getSprite, SECRET);
    
    //写真アニメーション
    _secretSprite->setScale(0.1f);
    ActionInterval* scaleAction1 = ScaleTo::create(0.25, 1.5);
    ActionInterval* ease1 = EaseOut::create(scaleAction1, 2);
    ActionInterval* scaleAction2 = ScaleTo::create(0.25, 0.95);
    ActionInterval* ease2 = EaseInOut::create(scaleAction2, 2);
    ActionInterval* scaleAction3 = ScaleTo::create(0.1, 1.0);
    Sequence* sequence1 = Sequence::create(ease1,ease2,scaleAction3, NULL);
    _secretSprite->runAction(sequence1);
    
    //文字アニメーション
    _getSprite->setScale(1.0, 0.3);
    _getSprite->setOpacity(0);
    ActionInterval* delayActionA = DelayTime::create(0.25);
    ActionInterval* fadeActionA = FadeIn::create(0.0);
    
    ActionInterval* scaleActionB = ScaleTo::create(0.2, 0.8, 1.5);
    ActionInterval* moveActionB = MoveBy::create(0.2, Point(0, 100));
    Spawn* spawnB = Spawn::create(scaleActionB, moveActionB, NULL);
    ActionInterval* easeB = EaseOut::create(spawnB, 2);
    
    ActionInterval* scaleActionC = ScaleTo::create(0.25, 1.1, 0.7);
    ActionInterval* moveActionC = MoveBy::create(0.25, Point(0, -50));
    Spawn* spawnC = Spawn::create(scaleActionC, moveActionC, NULL);
    ActionInterval* easeC = EaseInOut::create(spawnC, 2);
    
    ActionInterval* scaleActionD = ScaleTo::create(0.15, 1.0, 1.1);
    
    ActionInterval* scaleActionE = ScaleTo::create(0.15, 1.0);
    cocos2d::CallFunc *callbackE = CallFunc::create([this]()
                                                   {
                                                       makeCloseBtn();
                                                   });
    
    Sequence* sequenceA = Sequence::create(delayActionA, fadeActionA, easeB, easeC, scaleActionD, scaleActionE, callbackE, NULL);
    _getSprite->runAction(sequenceA);
}

void GetSecretLayer::makeCloseBtn()
{
    //閉じるボタン
    _closeButton = Button::create();
    _closeButton->setTouchEnabled(true);
    _closeButton->loadTextures("Graphics/sp/close_btn_off.png", "Graphics/sp/close_btn_on.png", "");
    _closeButton->setPosition(Point(475, 765));
    _closeButton->addTouchEventListener(CC_CALLBACK_2(GetSecretLayer::touchEvent, this));
    this->addChild(_closeButton, CLOSE);
    
    //保存
    cocos2d::UserDefault* user = cocos2d::UserDefault::getInstance();
    std::string secretKey = StringUtils::format("secretKey_%d", _secretNo);
    long len = secretKey.length();
    char* keyName = new char[len+1];
    memcpy(keyName, secretKey.c_str(), len+1);
    user->setBoolForKey(keyName, true);
}

void GetSecretLayer::touchEvent(Ref *pSender, Widget::TouchEventType type)
{
    if (type == Widget::TouchEventType::ENDED)
    {
        _secretSprite->removeFromParentAndCleanup(true);
        _getSprite->removeFromParentAndCleanup(true);
        _closeButton->removeFromParentAndCleanup(true);
        ActionInterval* fadeAction = FadeOut::create(0.4);
        cocos2d::CallFunc *callback = CallFunc::create([this]()
                                                       {
                                                           endGetAction();
                                                       });
        this->runAction(Sequence::create(fadeAction, callback, NULL));
    }
}

void GetSecretLayer::endGetAction()
{
    Scene *myScene = Director::getInstance()->getRunningScene();
    StageBaseScene *parentLayer = (StageBaseScene*)myScene->getChildByTag(PARENT_LAYER_TAG);
    parentLayer->gatSecret();
    
    this->removeFromParentAndCleanup(true);
}

void GetSecretLayer::saveStageNo(STAGE stage)
{
    _secretNo = stage;
}

