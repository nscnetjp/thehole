#include "stage08.h"

USING_NS_CC;
using namespace ui;

static const int MAX_PUNCH_COUNT = 8;
static const float SECRET_TIME = 3.0;
static const Point SECRET_POSITION = Point(520, 250);
static const float QUAKE_FLAME = 0.02;
static const float QUAKE_DISTANCE = 20.0;

Scene* Stage08::createScene()
{
    auto scene = Scene::create();
    
    auto layer = Stage08::create();
    layer->setTag(PARENT_LAYER_TAG);
    scene->addChild(layer);
    
    return scene;
}

bool Stage08::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
    setCommon(STAGE_08);
    
    _hitCount = 0;
    _gloveStatus = ITEM_FALL;
    _secretStatus = FIRST_STEP;
    
    //タッチ
    auto dispatcher = Director::getInstance()->getEventDispatcher();
    auto listener = EventListenerTouchOneByOne::create();
    listener->onTouchBegan = CC_CALLBACK_2(Stage08::onTouchBegan, this);
    dispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    
    auto roomSprite = Sprite::create("Graphics/08/room08.png");
    roomSprite->setAnchorPoint(Point::ZERO);
    roomSprite->setPosition(Point::ZERO);
    _stageLayer->addChild(roomSprite, ROOM);
    
    //おじさん
    _ojiHole= Sprite::create("Graphics/08/oji08_hole.png");
    _ojiHole->setPosition(Point(320, 500));
    _stageLayer->addChild(_ojiHole, OJISAN_1);
    _ojiHole->setVisible(false);
    //おじさん
    _oji = Sprite::create("Graphics/08/oji08.png");
    _oji->setPosition(Point(320, 460));
    _stageLayer->addChild(_oji, OJISAN_0);
    //グローブ
    _glove = Sprite::create("Graphics/08/glove08.png");
    _glove->setPosition(Point(75, 290));
    _stageLayer->addChild(_glove, GLOVE);
    //サンドバッグ
    _sandBag = Sprite::create("Graphics/08/sand_bag.png");
    _sandBag->setPosition(Point(133, 684));
    _stageLayer->addChild(_sandBag, SAND_BAG);
    //入手アイテム画像
    _item = Sprite::create("Graphics/08/glove08_off.png");
    _item->setPosition(Point(_getItemPosition.x,_getItemPosition.y - 200));
    _stageLayer->addChild(_item, ITEM_BTN);
    _item->setVisible(false);
    //シークレット画像
    _secret = Sprite::create("Graphics/08/secret08.png");
    _secret->setPosition(Point(320, 400));
    _stageLayer->addChild(_secret, ITEM_BTN);
    _secret->setVisible(false);
    //アイテムボタン
    _itemButton = Button::create();
    _itemButton->setTouchEnabled(false);
    _itemButton->loadTextures("Graphics/common/item_btn.png", "Graphics/common/item_btn.png", "");
    _itemButton->setPosition(_getItemPosition);
    _itemButton->addTouchEventListener(CC_CALLBACK_2(Stage08::touchEvent, this));
    _stageLayer->addChild(_itemButton, ITEM_BTN, ITEM_ACT);
    //クリアボタン
    _clearButton = Button::create();
    _clearButton->setTouchEnabled(false);
    _clearButton->loadTextures("Graphics/common/clear_btn.png", "Graphics/common/clear_btn.png", "");
    _clearButton->setPosition(Point(320, 530));
    _clearButton->addTouchEventListener(CC_CALLBACK_2(StageBaseScene::touchEvent, this));
    _stageLayer->addChild(_clearButton, CLEAR_BTN, STAGE_CLEAR_ACT);
    //シークレットボタン
    _secretButton = Button::create();
    _secretButton->setTouchEnabled(false);
    _secretButton->loadTextures("Graphics/common/secret_btn.png", "Graphics/common/secret_btn.png", "");
    _secretButton->setPosition(SECRET_POSITION);
    _secretButton->addTouchEventListener(CC_CALLBACK_2(Stage08::touchEvent, this));
    _stageLayer->addChild(_secretButton, SECRET_BTN, SECRET_ACT);
    
    return true;
}

bool Stage08::onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *unused_event)
{
    Point touchPoint = touch->getLocation();
    
    if (_gloveStatus == ITEM_FALL)
    {
        Rect gloveRect = _glove->boundingBox();
        
        if(gloveRect.containsPoint(touchPoint))
        {
            getItem();
            _glove->removeFromParentAndCleanup(true);
            _gloveStatus = ITEM_UNUSE;
        }
    }
    else if (_gloveStatus == ITEM_USE)
    {
        if (_hitCount <= MAX_PUNCH_COUNT)
        {
            Point ojiPosition = _oji->getPosition();
            Rect ojiHitArea = Rect(ojiPosition.x - 80, ojiPosition.y, 160, 210);
            Rect sandBag = _sandBag->boundingBox();
            
            if(ojiHitArea.containsPoint(touchPoint)) //おじさん
            {
                ojiPunch();
                
                if (_secretStatus == FIRST_STEP)
                {
                    _secretStatus = START;
                    this->scheduleOnce(schedule_selector(Stage08::timeLimit), SECRET_TIME);
                }
            }
            else if (sandBag.containsPoint(touchPoint))
            {
                ActionInterval* scaleAct0 = ScaleBy::create(0.03, 1.05);
                ActionInterval* scaleAct1 = ScaleTo::create(0.03, 1.0);
                _sandBag->runAction(Sequence::create(scaleAct0, scaleAct1, nullptr));
            }
        }
    }
    
    return true;
}

void Stage08::touchEvent(Ref *pSender, Widget::TouchEventType type)
{
    Node *node = dynamic_cast<Node *>(pSender);
    int buttonTag = node->getTag();
    //アイテムボタンの切り替え
    if (buttonTag == ITEM_ACT)
    {
        switch (type)
        {
            case Widget::TouchEventType::BEGAN:
                if (_gloveStatus == ITEM_UNUSE)
                {
                    _item->setTexture("Graphics/08/glove08_on.png");
                }
                break;
                
            case Widget::TouchEventType::ENDED:
                if (_gloveStatus == ITEM_USE)
                {
                    _gloveStatus = ITEM_UNUSE;
                    _item->setTexture("Graphics/08/glove08_off.png");
                }
                else if (_gloveStatus == ITEM_UNUSE)
                {
                    _gloveStatus = ITEM_USE;
                }
                break;
                
            case Widget::TouchEventType::CANCELED:
                if (_gloveStatus == ITEM_UNUSE)
                {
                    _gloveStatus = ITEM_USE;
                }
                
                break;
                
            default:
                break;
        }
    }
    else if (buttonTag == SECRET_ACT)
    {
        _secretButton->removeFromParentAndCleanup(true);
        _secret->removeFromParentAndCleanup(true);
        getSecret();
    }
}

void Stage08::getItem()
{
    _item->setVisible(true);
    _item->runAction(EaseOut::create(JumpTo::create(0.2, _getItemPosition, 100, 1), 2));
    _itemButton->setTouchEnabled(true);
    
}

void Stage08::ojiPunch()
{
    _hitCount++;
    
    if (_hitCount < MAX_PUNCH_COUNT)
    {
        Point ojiPosition[3] =
        {
            Point(320, 460), //真ん中
            Point(120, 460), //左
            Point(520, 460), //右
        };
        
        int positionID = 0;
        
        if (_hitCount < MAX_PUNCH_COUNT - 2)
        {
            do {
                positionID = rand() % 3;
                
            } while (ojiPosition[positionID] == _oji->getPosition());
        }
        else if (_hitCount == MAX_PUNCH_COUNT - 2)
        {
            do {
                positionID = rand() % 2 + 1;
                
            } while (ojiPosition[positionID] == _oji->getPosition());
        }
        else if (_hitCount == MAX_PUNCH_COUNT - 1)
        {
            positionID = 0;
        }
        
        _oji->runAction(MoveTo::create(0.05, ojiPosition[positionID]));
    }
    else if (_hitCount == MAX_PUNCH_COUNT)
    {
        float hitActDuration = 0.3;
        _oji->setTexture("Graphics/08/oji08_hit.png");
        ActionInterval* moveAct = MoveTo::create(hitActDuration, _ojiHole->getPosition());
        ActionInterval* scaleAct = ScaleBy::create(hitActDuration, 0.8);
        Spawn* spawn = Spawn::create(moveAct, scaleAct, nullptr);
        cocos2d::CallFunc *firstHitCallback = CallFunc::create([this]()
                                                        {
                                                            ojiHit();
                                                        });
        _oji->runAction(Sequence::create(spawn, firstHitCallback, nullptr));
        
        if (_secretStatus == START)
        {
            ActionInterval* jumpAct = JumpTo::create(0.7, SECRET_POSITION, 300, 1);
            cocos2d::CallFunc *secretCallback = CallFunc::create([this]()
                                                                 {
                                                                     secretBtnEnabled();
                                                                 });
            _secret->setVisible(true);
            _secret->runAction(Sequence::create(jumpAct, secretCallback, nullptr));
        }
    }
}

void Stage08::ojiHit()
{
    _oji->setVisible(false);
    _ojiHole->setVisible(true);
    _clearButton->setTouchEnabled(true);
    //画面の揺れ
    ActionInterval* startScale = ScaleTo::create(QUAKE_FLAME, 1.02);
    ActionInterval* upMove = MoveTo::create(QUAKE_FLAME, Point(0, QUAKE_DISTANCE));
    ActionInterval* downMove = MoveTo::create(QUAKE_FLAME, Point(0, - QUAKE_DISTANCE));
    ActionInterval* rightMove = MoveTo::create(QUAKE_FLAME, Point(QUAKE_DISTANCE, 0));
    ActionInterval* lefttMove = MoveTo::create(QUAKE_FLAME, Point(- QUAKE_DISTANCE, 0));
    ActionInterval* endMove = MoveTo::create(QUAKE_FLAME, Point::ZERO);
    ActionInterval* endScale = ScaleTo::create(QUAKE_FLAME, 1.0);
    Spawn* endSpawn = Spawn::create(endMove, endScale, nullptr);
    
    _stageLayer->runAction(Sequence::create(startScale, upMove, downMove, lefttMove, rightMove, endSpawn, nullptr));
}

void Stage08::timeLimit(float tm)
{
    _secretStatus = LIMIT;
}

void Stage08::secretBtnEnabled()
{
    _secretButton->setTouchEnabled(true);
}
