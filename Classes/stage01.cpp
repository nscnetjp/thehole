#include "stage01.h"

USING_NS_CC;
using namespace ui;

static const float QUAKE_FLAME = 0.02;
static const float QUAKE_DISTANCE = 5.0;

Scene* Stage01::createScene()
{
    auto scene = Scene::create();
    
    auto layer = Stage01::create();
    layer->setTag(PARENT_LAYER_TAG);
    scene->addChild(layer);
    
    return scene;
}

bool Stage01::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
    setCommon(STAGE_01);
    
    _secretStatus = BTN_HIDDEN;
    
    auto dispatcher = Director::getInstance()->getEventDispatcher();
    auto listener = EventListenerTouchOneByOne::create();
    listener->onTouchBegan = CC_CALLBACK_2(Stage01::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(Stage01::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(Stage01::onTouchEnded, this);
    dispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    
    _stageLayer0 = Layer::create();
    _stageLayer->addChild(_stageLayer0,STAGE_LAYER_0);
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Point origin = Director::getInstance()->getVisibleOrigin();
    //床部分
    auto roomSprite = Sprite::create("Graphics/01/room01_1.png");
    roomSprite->setAnchorPoint(Point::ZERO);
    roomSprite->setPosition(Point::ZERO);
    _stageLayer->addChild(roomSprite, ROOM);
    //背景
    auto room0 = Sprite::create("Graphics/01/room01_0.png");
    room0->setAnchorPoint(Point::ZERO);
    room0->setPosition(Point::ZERO);
    _stageLayer0->addChild(room0, ROOM_0);
    //シークレット出現させるボタン
    _secretBtn = Sprite::create("Graphics/01/secretBtn01.png");
    _secretBtn->setPosition(Point(320, -130 + visibleSize.height + origin.y));
    _stageLayer0->addChild(_secretBtn, ROOM);
    //排気口
    _secretCover = Sprite::create("Graphics/01/exhaustPort01_0.png");
    _secretCover->setPosition(Point(320, -130 + visibleSize.height + origin.y));
    _stageLayer0->addChild(_secretCover, ROOM);
    //シークレット画像
    _secret = Sprite::create("Graphics/01/secret01.png");
    _secret->setPosition(Point(420, 190));
    _stageLayer0->addChild(_secret, SECRET);
    //クリアボタン
    Button* clearButton = Button::create();
    clearButton->setTouchEnabled(true);
    clearButton->loadTextures("Graphics/common/clear_btn.png", "Graphics/common/clear_btn.png", "");
    clearButton->setPosition(Point(325, 580));
    clearButton->addTouchEventListener(CC_CALLBACK_2(StageBaseScene::touchEvent, this));
    _stageLayer0->addChild(clearButton, CLEAR_BTN, STAGE_CLEAR_ACT);
    //シークレットボタン
    _secretButton = Button::create();
    _secretButton->setTouchEnabled(false);
    _secretButton->loadTextures("Graphics/common/secret_btn.png", "Graphics/common/secret_btn.png", "");
    _secretButton->setPosition(_secret->getPosition());
    _secretButton->addTouchEventListener(CC_CALLBACK_2(Stage01::touchEvent, this));
    _stageLayer0->addChild(_secretButton, CLEAR_BTN);
    
    return true;
}

bool Stage01::onTouchBegan(Touch *touch, Event *event)
{
    Point point = touch->getLocation();
    Rect  rect = _secretBtn->boundingBox();
    
    if(rect.containsPoint(point))
    {
        if (_secretStatus == BTN_HIDDEN)
        {
            _secretStatus = CATCH_COVER;
            _catchCoverPoint = _secretCover->getPosition() - touch->getLocation();
        }
        else if (_secretStatus == COVER_FALL)
        {
            _secretStatus = PUSHED_BTN;
            secretAct();
        }
    }
    return true;
}

void Stage01::onTouchMoved(Touch *touch, Event *event)
{
    if (_secretStatus == CATCH_COVER)
    {
        Point coverPosition = touch->getLocation() + _catchCoverPoint;
        _secretCover->setPosition(coverPosition);
    }
}

void Stage01::onTouchEnded(Touch *touch, Event *event)
{
    if (_secretStatus == CATCH_COVER)
    {
        if (_secretCover->getPosition() == _secretBtn->getPosition())
        {
            _secretStatus = BTN_HIDDEN;
        }
        else
        {
            _secretStatus = COVER_FALL;
            _secretCover->setRotation(10);
            scheduleUpdate();
        }
    }
}

void Stage01::update(float dt)
{
    _coverSpeedY += 50.0;
    Point coverPosition = _secretCover->getPosition();
    coverPosition.y -= _coverSpeedY * dt;
    
    if (coverPosition.y > 310)
    {
        _secretCover->setPosition(coverPosition);
    }
    else
    {
        coverPosition.y = 310;
        _secretCover->setPosition(coverPosition);
        _secretCover->setRotation(0);
        _secretCover->setTexture("Graphics/01/exhaustPort01_1.png");
        unscheduleUpdate();
    }
}

void Stage01::touchEvent(Ref *pSender, Widget::TouchEventType type)
{
    if (type == Widget::TouchEventType::ENDED)
    {
        getSecret();
        _secret->removeFromParentAndCleanup(true);
        _secretButton->removeFromParentAndCleanup(true);
    }
}

void Stage01::secretTouchEnabled()
{
    _secretButton->setTouchEnabled(true);
    _stageLayer->stopAllActions();
    ActionInterval* endMove = MoveTo::create(0.2, Point::ZERO);
    ActionInterval* endScale = ScaleTo::create(0.2, 1.0);
    _stageLayer->runAction(Spawn::create(endMove, endScale, nullptr));
}

void Stage01::secretAct()
{
    
    ActionInterval* startScale = ScaleTo::create(QUAKE_FLAME, 1.02);
    ActionInterval* upMove = MoveTo::create(QUAKE_FLAME, Point(0, QUAKE_DISTANCE));
    ActionInterval* downMove = MoveTo::create(QUAKE_FLAME, Point(0, - QUAKE_DISTANCE));
    ActionInterval* rightMove = MoveTo::create(QUAKE_FLAME, Point(QUAKE_DISTANCE, 0));
    ActionInterval* lefttMove = MoveTo::create(QUAKE_FLAME, Point(- QUAKE_DISTANCE, 0));
    Sequence* actSequence = Sequence::create(downMove, upMove, downMove, lefttMove, downMove, upMove, downMove, rightMove, nullptr);
    Repeat* actRepeat = Repeat::create(actSequence, 100);
    
    _stageLayer->runAction(Sequence::create(startScale, actRepeat, nullptr));
    
    ActionInterval* delayAct = DelayTime::create(1.0);
    ActionInterval* moveAct = MoveBy::create(3.5, Point(0, 60));
    cocos2d::CallFunc *callback = CallFunc::create([this]()
                                                   {
                                                       secretTouchEnabled();
                                                   });
    _stageLayer0->runAction(Sequence::create(delayAct, moveAct, callback, nullptr));
}
