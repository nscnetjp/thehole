#include "stage05.h"

USING_NS_CC;
using namespace ui;

static const float FIRST_OPEN= 4.5;
static const float FIRST_CLOSE = FIRST_OPEN + 0.6;
static const float SECOND_OPEN = FIRST_CLOSE + 3.6;
static const float SECOND_CLOSE = SECOND_OPEN + 3.0;

Scene* Stage05::createScene()
{
    auto scene = Scene::create();
    
    auto layer = Stage05::create();
    layer->setTag(PARENT_LAYER_TAG);
    scene->addChild(layer);
    
    return scene;
}

bool Stage05::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
    setCommon(STAGE_05);
    
    _secretTime = -1.0;
    _mouthStatus = CLOSE;
    _secretStatus = SECRET_CLOSE_0;
    
    //タッチ
    auto dispatcher = Director::getInstance()->getEventDispatcher();
    auto listener = EventListenerTouchOneByOne::create();
    listener->onTouchBegan = CC_CALLBACK_2(Stage05::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(Stage05::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(Stage05::onTouchEnded, this);
    dispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    
    //壁断面
    auto sectionSprite = Sprite::create("Graphics/05/room05_1.png");
    sectionSprite->setPosition(Point(317, 402));
    _stageLayer->addChild(sectionSprite, SECTION);
    //壁の下あご部分
    _chinWall = Sprite::create("Graphics/05/room05_0.png");
    _chinWall->setPosition(Point(317, 402));
    _stageLayer->addChild(_chinWall, CHIN_WALL);
    //背景
    auto roomSprite = Sprite::create("Graphics/05/room05.png");
    roomSprite->setAnchorPoint(Point::ZERO);
    roomSprite->setPosition(Point::ZERO);
    _stageLayer->addChild(roomSprite, ROOM);
    //目
    _eyeWall = Sprite::create("Graphics/05/eye05_0.png");
    _eyeWall->setPosition(Point(309, 738));
    _stageLayer->addChild(_eyeWall, EYE_WALL);
    //排気口
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Point origin = Director::getInstance()->getVisibleOrigin();
    auto exhaustPort = Sprite::create("Graphics/common/exhaustPort.png");
    exhaustPort->setPosition(Point(40, -140 + visibleSize.height + origin.y));
    _stageLayer->addChild(exhaustPort, ROOM);
    //クリアボタン
    _clearButton = Button::create();
    _clearButton->setTouchEnabled(false);
    _clearButton->loadTextures("Graphics/common/clear_btn.png", "Graphics/common/clear_btn.png", "");
    _clearButton->setPosition(Point(310, 440));
    _clearButton->addTouchEventListener(CC_CALLBACK_2(StageBaseScene::touchEvent, this));
    _stageLayer->addChild(_clearButton, CLEAR_BTN, STAGE_CLEAR_ACT);
    
    //シークレット画像
    _secret = Sprite::create("Graphics/05/secret05.png");
    _secret->setPosition(Point(190, 720));
    _stageLayer->addChild(_secret, SECRET);
    _secret->setVisible(false);
    //シークレットボタン
    _secretButton = Button::create();
    _secretButton->setTouchEnabled(false);
    _secretButton->loadTextures("Graphics/common/secret_btn.png", "Graphics/common/secret_btn.png", "");
    _secretButton->setPosition(Point(190, 250));
    _secretButton->addTouchEventListener(CC_CALLBACK_2(Stage05::touchEvent, this));
    _stageLayer->addChild(_secretButton, SECRET_BTN);
    
    scheduleUpdate();
    
    return true;
}

bool Stage05::onTouchBegan(Touch *touch, Event *event)
{
    if (_mouthStatus == CLOSE)
    {
        Rect chinWallRect = _chinWall->boundingBox();
        if(chinWallRect.containsPoint(touch->getLocation()))
        {
            _touchBeganPoint = touch->getLocation();
            _mouthStatus = BEGIN_OPEN;
        }
    }
    
    if (_secretStatus != SECRET_END)
    {
        _secretStatus = SECRET_RESET;
        
        if (_secretStatus == SECRET_OPEN)
        {
            _secretTime = 0.0;
            
            auto animate = Animation::create();
            std::string ojiAnimeImg[3] =
            {
                "Graphics/05/eye05_1_1.png",
                "Graphics/05/eye05_1_0.png",
                "Graphics/05/eye05_0.png"
            };
            for (int i = 0; i < 3; i++)
            {
                animate->addSpriteFrameWithFile(ojiAnimeImg[i]);
            }
            animate->setDelayPerUnit(0.1);
            _eyeWall->runAction(Animate::create(animate));
        }
    }
    
    return true;
}

void Stage05::onTouchMoved(Touch *touch, Event *event)
{
    if (_mouthStatus == BEGIN_OPEN)
    {
        if (touch->getLocation().y - _touchBeganPoint.y < - 100)
        {
            _mouthStatus = OPEN;
            ActionInterval* moveAct = EaseInOut::create(MoveBy::create(1.6, Point(0, -110)), 2.5);
            cocos2d::CallFunc *callback = CallFunc::create([this]()
                                                           {
                                                               holeInEnabled();
                                                           });
            _chinWall->runAction(Sequence::create(moveAct, callback, nullptr));
        }
        
        Rect chinWallRect = _chinWall->boundingBox();
        if(!chinWallRect.containsPoint(touch->getLocation()))
        {
            _mouthStatus = CLOSE;
        }
    }
}

void Stage05::onTouchEnded(Touch *touch, Event *event)
{
    if (_mouthStatus != OPEN)
    {
        _mouthStatus = CLOSE;
    }
    
    if (_secretStatus == SECRET_RESET)
    {
        _secretStatus = SECRET_CLOSE_0;
        _secretTime = 0.0;
    }
}

void Stage05::touchEvent(Ref *pSender, Widget::TouchEventType type)
{
    if (type == Widget::TouchEventType::ENDED)
    {
        _secret->removeFromParentAndCleanup(true);
        _secretButton->removeFromParentAndCleanup(true);
        
        getSecret();
    }
}

void Stage05::update(float dt)
{
    if (_secretStatus != SECRET_RESET)
    {
        _secretTime += dt;
        
        if (_secretTime > FIRST_OPEN &&
            _secretStatus == SECRET_CLOSE_0)
        {
            _secretStatus = SECRET_OPEN;
            
            auto animate = Animation::create();
            std::string ojiAnimeImg[2] =
            {
                "Graphics/05/eye05_1_0.png",
                "Graphics/05/eye05_1_1.png"
            };
            for (int i = 0; i < 2; i++)
            {
                animate->addSpriteFrameWithFile(ojiAnimeImg[i]);
            }
            animate->setDelayPerUnit(0.1);
            _eyeWall->runAction(Animate::create(animate));
        }
        else if (_secretTime > FIRST_CLOSE &&
                 _secretStatus == SECRET_OPEN)
        {
            _secretStatus = SECRET_CLOSE_1;
            
            auto animate = Animation::create();
            std::string ojiAnimeImg[2] =
            {
                "Graphics/05/eye05_1_0.png",
                "Graphics/05/eye05_0.png"
            };
            for (int i = 0; i < 2; i++)
            {
                animate->addSpriteFrameWithFile(ojiAnimeImg[i]);
            }
            animate->setDelayPerUnit(0.1);
            _eyeWall->runAction(Animate::create(animate));
        }
        else if (_secretTime > SECOND_OPEN &&
                 _secretStatus == SECRET_CLOSE_1)
        {
            _secretStatus = SECRET_END;
            
            auto animate = Animation::create();
            std::string ojiAnimeImg[7] =
            {
                "Graphics/05/eye05_2_0.png",
                "Graphics/05/eye05_2_1.png",
                "Graphics/05/eye05_2_0.png",
                "Graphics/05/eye05_0.png",
                "Graphics/05/eye05_2_0.png",
                "Graphics/05/eye05_2_1.png",
                "Graphics/05/eye05_2_2.png"
            };
            for (int i = 0; i < 7; i++)
            {
                animate->addSpriteFrameWithFile(ojiAnimeImg[i]);
            }
            animate->setDelayPerUnit(0.1);
            _eyeWall->runAction(Animate::create(animate));
            
            _secret->setVisible(true);
            ActionInterval* moveAct = EaseIn::create(MoveTo::create(0.6, _secretButton->getPosition()), 2);
            cocos2d::CallFunc *callback = CallFunc::create([this]()
                                                           {
                                                               secretBtnEnabled();
                                                           });
            _secret->runAction(Sequence::create(moveAct, callback, nullptr));
        }
        else if (_secretTime > SECOND_CLOSE &&
                 _secretStatus == SECRET_END)
        {
            unscheduleUpdate();
            
            auto animate = Animation::create();
            std::string ojiAnimeImg[3] =
            {
                "Graphics/05/eye05_2_1.png",
                "Graphics/05/eye05_2_0.png",
                "Graphics/05/eye05_0.png"
            };
            for (int i = 0; i < 3; i++)
            {
                animate->addSpriteFrameWithFile(ojiAnimeImg[i]);
            }
            animate->setDelayPerUnit(0.1);
            _eyeWall->runAction(Animate::create(animate));
        }
    }
}

void Stage05::holeInEnabled()
{
    _clearButton->setTouchEnabled(true);
}

void Stage05::secretBtnEnabled()
{
    _secretButton->setTouchEnabled(true);
}
