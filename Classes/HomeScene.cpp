#include "HomeScene.h"
#include "NativeBridge.h"

USING_NS_CC;
using namespace ui;

static const float FADE_IN_DURATION = 0.2f;
static const float STAGE_SELECT_DURATION = 0.4f;
static const float HOLE_IN_DURATION = 0.5f;

Scene* HomeScene::createScene()
{
    auto scene = Scene::create();
    
    auto layer = HomeScene::create();
    
    scene->addChild(layer);
    
    return scene;
}

// on "init" you need to initialize your instance
bool HomeScene::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
    _btnFlag = true;
    
    Point origin = Director::getInstance()->getVisibleOrigin();
    
    //セーブデータを読み込む
    cocos2d::UserDefault* user = cocos2d::UserDefault::getInstance();
    //TODO:セーブデータまっさらにするためのもの（後で消す） ---------------//
//    user->setIntegerForKey("clearStageKey", 0);
//    for (int i = 0; i < 10; i++)
//    {
//        std::string secretKey = StringUtils::format("secretKey_%d", i);
//        long len = secretKey.length();
//        char* keyName = new char[len+1];
//        memcpy(keyName, secretKey.c_str(), len+1);
//        user->setBoolForKey(keyName, false);
//    }
    //-------------------------------------------------------------//
    
    //乱数系列の変更
    srand((unsigned int)time(NULL));
    
    int clearStage = user->getIntegerForKey("clearStageKey", 0);
    
    //ホームのトップ画面を作る
    _homeTopLayer = HomeTopLayer::create();
    this->addChild(_homeTopLayer, TOP_PAGE);
    _homeTopLayer->setBackgroundImg(clearStage);
    
    //ボタンを設置する
    std::string buttonFIleName[3]
    {
        "Graphics/top/nsc.png",
        "Graphics/top/start.png",
        "Graphics/top/stage_select.png"
    };
    
    Point buttonPosition[3]
    {
        Point(515, 420),
        Point(320, 565),
        Point(125, 300)
    };
    
    int loadBtn;
    
    if (clearStage == 0) loadBtn = 2;
    else loadBtn = 3;
    
    for (int i = 0; i < loadBtn; i++)
    {
        Button* startButton = Button::create();
        startButton->setTouchEnabled(true);
        
        long len = buttonFIleName[i].length();
        char* btnName = new char[len+1];
        memcpy(btnName, buttonFIleName[i].c_str(), len+1);
        
        startButton->loadTextures(btnName, btnName, "");
        startButton->setPosition(buttonPosition[i]);
        startButton->addTouchEventListener(CC_CALLBACK_2(HomeScene::touchEvent, this));
        _homeTopLayer->addChild(startButton, 0, i);
    }
    
    //フッター
    auto footerSprite = Sprite::create("Graphics/common/footer.png");
    footerSprite->setAnchorPoint(Point::ZERO);
    footerSprite->setPosition(Point(origin.x, origin.y));
    addChild(footerSprite,FOOTER);
    
    //TODO:バナー広告    
//    char apiKey = "a6eca9dd074372c898dd1df549301f277c53f2b9";
//    char spotID = "3172";
//    NendModule::createNADViewBottom(apiKey, spotID);
//    NendIconModule::load();
    
    //セーブデータがある場合ステージ選択画面読み込む
    if (clearStage)
    {
        _stageSelectBaseLayer = StageSelectBaseLayer::create();
        _stageSelectBaseLayer->setPosition(Point(0, -1140 + origin.y));
        addChild(_stageSelectBaseLayer, STAGE_SELECT);
        //穴ボタンを表示
        clearStage = MIN(9, clearStage);
        _stageSelectBaseLayer->setHole(clearStage);
    }
    
    //シークレット
    int secretCount = 0;
    for (int i = 0; i < 10; i++)
    {
        std::string secretKey = StringUtils::format("secretKey_%d", i);
        long len = secretKey.length();
        char* keyName = new char[len+1];
        memcpy(keyName, secretKey.c_str(), len+1);
        bool secret = user->getBoolForKey(keyName, false);
        if (secret)
        {
            secretCount++;
        }
    }
    Rect numberRect[15];
    float rectSide = 100;
    if (secretCount < 10)
    {
        int i = 0;
        for (int y = 0; y < 3; y++)
        {
            for (int x = 0; x < 5; x++)
            {
                numberRect[i] = Rect(rectSide * x, rectSide * y, rectSide, rectSide);
                i++;
            }
        }
        
    }
    else
    {
        auto secretSprite = Sprite::create("Graphics/top/footer.png"); //TODO:シークレット画像に変える
        secretSprite->setAnchorPoint(Point::ZERO);
        secretSprite->setPosition(Point(origin.x, origin.y));
        addChild(secretSprite,FOOTER);
    }
    
    //表示時の演出
    startDirect();
    
    return true;
}

void HomeScene::startDirect()
{
    _homeTopLayer->setScale(1.05f);
    _homeTopLayer->runAction(ScaleTo::create(FADE_IN_DURATION, 1.0f));
    //遷移するときの暗転用の画像
    _topPageShade = Sprite::create("Graphics/common/black.png");
    _topPageShade->setAnchorPoint(Point::ZERO);
    _topPageShade->setPosition(Point::ZERO);
    addChild(_topPageShade, TOP_PAGE_SHADE);
    _topPageShade->runAction(FadeOut::create(0.3f));
}

void HomeScene::touchEvent(Ref *pSender, Widget::TouchEventType type)
{
    if (_btnFlag == true && type == Widget::TouchEventType::ENDED)
    {
        Node *node = dynamic_cast<Node *>(pSender);
        int buttonTag = node->getTag();
        
        switch (buttonTag)
        {
            case NSC_BTN:
                platform::NativeBridge::nscJumpNative();
                break;
            case START_BTN:
                repraceIntro();
                break;
            case STAGE_SELECT_BTN:
                _btnFlag = false;
                HomeScene::stageSelect();
                break;
            default:
                break;
        }
    }
}

void HomeScene::stageSelect()
{
    //ホームのトップを上にアニメーション
    _homeTopLayer->runAction(MoveTo::create(STAGE_SELECT_DURATION, Point(Point(0, 400))));
    //暗転のアニメーション
    _topPageShade->runAction(FadeIn::create(STAGE_SELECT_DURATION));
    
    //ステージセレクト画面を下からアニメーション
    ActionInterval* moveAction = MoveTo::create(STAGE_SELECT_DURATION, Point(Point::ZERO));
    ActionInterval* easeAction = EaseIn::create(moveAction, 1.5);
    ActionInterval* jumpAction = JumpBy::create(0.2, Point::ZERO, 20.0f, 2);
    cocos2d::CallFunc *callback = CallFunc::create([this]()
                                                   {
                                                       _stageSelectBaseLayer->startScroll();
                                                   });
    _stageSelectBaseLayer->runAction(Sequence::create(easeAction, jumpAction, callback, nullptr));
}

void HomeScene::repraceIntro()
{
    //拡大
    this->runAction(ScaleBy::create(HOLE_IN_DURATION + 0.05f, 1.3f));
    //黒丸画像
    auto holeInSprite = Sprite::create("Graphics/common/holein.png");
    holeInSprite->setPosition(Point(320, 570));
    addChild(holeInSprite, TOP_PAGE_SHADE);
    holeInSprite->runAction(ScaleBy::create(HOLE_IN_DURATION + 0.05f, 5.0f));
    //暗転して次のステージへ遷移
    ActionInterval* fadeAct = FadeIn::create(HOLE_IN_DURATION);
    CallFunc *callback = CallFunc::create([this]()
                                                   {
                                                       ReplaceStageClass::replaceStage(STAGE_01);
                                                   });
    _topPageShade->runAction(Sequence::create(fadeAct, callback, nullptr));
    _btnFlag = false;
}
