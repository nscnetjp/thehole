#ifndef __TITLE_SCENE__
#define __TITLE_SCENE__

#include "cocos2d.h"
#include "HomeTopLayer.h"
#include "StageSelectBaseLayer.h"
#include "ReplaceStage.h"
#include <ui/UIButton.h>

class HomeScene : public cocos2d::Layer
{
public:
    static cocos2d::Scene* createScene();
    virtual bool init();
    CREATE_FUNC(HomeScene);
private:
    enum HOME_LAYER_LAP
    {
        TOP_PAGE = 0,
        TOP_PAGE_SHADE,
        SECRET_IMG,
        STAGE_SELECT,
        FOOTER
    };
    
    enum BUTTON_TAG
    {
        NSC_BTN = 0,
        START_BTN,
        STAGE_SELECT_BTN
    };
    
    void startDirect();
    void touchEvent(Ref* pSender, cocos2d::ui::Widget::TouchEventType type);
    void stageSelect();
    void repraceIntro();
    
    cocos2d::Sprite *_topPageShade;
    HomeTopLayer *_homeTopLayer;
    StageSelectBaseLayer *_stageSelectBaseLayer;
    
    bool _btnFlag;
};

#endif /* defined(__TITLE_SCENE__) */
