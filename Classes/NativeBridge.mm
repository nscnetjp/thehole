#import "NativeBridge.h"
#import "NativeController_objc.h"
#import "platform/CCDevice.h"

namespace platform
{
    
    void NativeBridge::executeNative()
    {
        [NativeController_objc executeObjc];
    }
    
    void NativeBridge::nscJumpNative()
    {
        [NativeController_objc nscJumpObjc];
    }
    
    void NativeBridge::setBanner()
    {
        [NativeController_objc setBannerObjc];
    }
    
}
