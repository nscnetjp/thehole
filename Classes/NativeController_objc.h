#ifndef __TheHole__NativeController_objc__
#define __TheHole__NativeController_objc__

#import <Foundation/Foundation.h>
#import <CoreMotion/CoreMotion.h>

@interface NativeController_objc : NSObject

+ (void)executeObjc;
+ (void)nscJumpObjc;
+ (void)setBannerObjc;

@end

#endif /* defined(__TheHole__NativeController_objc__) */
