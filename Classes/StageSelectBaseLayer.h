#ifndef __MyCppGame__StageSelectBaseLayer__
#define __MyCppGame__StageSelectBaseLayer__

#include "cocos2d.h"
#include "StageSelectLayer.h"
#include "ReplaceStage.h"
#include <ui/UIButton.h>

class StageSelectBaseLayer : public cocos2d::Layer
{
public:
    virtual bool init();
    CREATE_FUNC(StageSelectBaseLayer);
    bool onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* event);
    void onTouchMoved(cocos2d::Touch* touch, cocos2d::Event* event);
    void onTouchEnded(cocos2d::Touch* touch, cocos2d::Event* event);
    void touchEvent(Ref* pSender, cocos2d::ui::Widget::TouchEventType type);
    
    void startScroll();
    void setHole(int stage);
    
private:
    enum LAYER_LAP
    {
        BACKGROUND = 0,
        HOLE,
        HOLE_IN,
        SHADE
    };
    virtual void update(float dt);
    void holeInReplace();
    
    StageSelectLayer *_stageSelectLayer;
    
    cocos2d::Point _layerPosition;
    float _touchPosition;
    float _touchMovedPosition;
    float _bgPosition;
    float _speed;
    int _btnTag;
    bool _touchFlag;
    bool _btnFlag;
};

#endif /* defined(__MyCppGame__StageSelectBaseLayer__) */
