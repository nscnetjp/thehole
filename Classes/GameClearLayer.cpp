#include "GameClearLayer.h"

USING_NS_CC;
using namespace ui;

Scene* GameClearScene::createScene()
{
    auto scene = Scene::create();
    
    auto layer = GameClearScene::create();
    layer->setTag(PARENT_LAYER_TAG);
    scene->addChild(layer);
    
    return scene;
}

bool GameClearScene::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Point origin = Director::getInstance()->getVisibleOrigin();
    
    //ステージレイヤー（このレイヤーにゲーム画面を作る）
    _stageLayer = Layer::create();
    addChild(_stageLayer);
    
    _repraceFlag = true;
    _stageNo = GAME_CLEAR;
    _secretCount = 0;
    
    
    //ヘッダー背景
    auto headerSprite = Sprite::create("Graphics/common/header.png");
    headerSprite->setAnchorPoint(Point::ANCHOR_TOP_LEFT);
    headerSprite->setPosition(Point(origin.x, visibleSize.height + origin.y));
    addChild(headerSprite,HEADER);
    //ヘッダーボタン(HOME)
    Button* homeButton = Button::create();
    homeButton->setTouchEnabled(true);
    homeButton->loadTextures("Graphics/common/home_off.png", "Graphics/common/home_on.png", "");
    homeButton->setAnchorPoint(Point::ANCHOR_TOP_LEFT);
    homeButton->setPosition(Point(10 + origin.x, visibleSize.height + origin.y));
    homeButton->addTouchEventListener(CC_CALLBACK_2(StageBaseScene::touchEvent, this));
    this->addChild(homeButton, HEADER_BTN, TO_HOME_ACT);
    //フッター
    auto footerSprite = Sprite::create("Graphics/common/footer.png");
    footerSprite->setAnchorPoint(Point::ZERO);
    footerSprite->setPosition(Point(origin.x, origin.y));
    addChild(footerSprite,FOOTER);
    
    //TODO:バナー広告
    
    auto roomSprite = Sprite::create("Graphics/clear/gameClear.png");
    roomSprite->setAnchorPoint(Point::ZERO);
    roomSprite->setPosition(Point::ZERO);
    _stageLayer->addChild(roomSprite, BACKGROUND);
    
    //真っ暗な状態からフェードイン
    _shadeSprite = Sprite::create("Graphics/common/black.png");
    _shadeSprite->setAnchorPoint(Point::ZERO);
    _shadeSprite->setPosition(Point::ZERO);
    addChild(_shadeSprite, SHADE);
    ActionInterval* delayAct = DelayTime::create(1.0);
    ActionInterval* fadeAct = FadeOut::create(1.5);
    CallFunc *callback = CallFunc::create([this]()
                                          {
                                              loadSecret();
                                          });
    _shadeSprite->runAction(Sequence::create(delayAct, fadeAct, callback, NULL));
    
    return true;
}

void GameClearScene::loadSecret()
{
    //全取得シークレットを確認
    UserDefault* user = cocos2d::UserDefault::getInstance();
    for (int i = 0; i < 10; i++)
    {
        std::string secretKey = StringUtils::format("secretKey_%d", i);
        long len = secretKey.length();
        char* keyName = new char[len+1];
        memcpy(keyName, secretKey.c_str(), len+1);
        bool secret = user->getBoolForKey(keyName, false);
        if (secret)
        {
            _secretCount++;
        }
    }
    log("secret:%d",_secretCount);
    if (_secretCount < 10)
    {
        this->scheduleOnce(schedule_selector(GameClearScene::normalClear), 0.8);
    }
    else
    {
        this->scheduleOnce(schedule_selector(GameClearScene::getAllSecretClear), 0.8);
    }
}

void GameClearScene::normalClear(float tm)
{
    //TODO:通常クリア演出(シークレットの説明入れる)
    //手に入れたシークレットの数の画像
    Rect numImgRect[10];
    const int WIDTH_SIZE  = 100; // 一つのスプライトの幅
    const int HEIGHT_SIZE = 100;
    for (int y = 0; y < 2; y++)
    {
        for (int x = 0; x < 5; x++)
        {
            numImgRect[x + (5 * y)] = Rect(WIDTH_SIZE * x, HEIGHT_SIZE * y, WIDTH_SIZE, HEIGHT_SIZE);
        }
    }
    auto getSecretNumSprite = Sprite::create("Graphics/common/number.png", numImgRect[_secretCount]);
    getSecretNumSprite->setPosition(Point(320, 250));
    _stageLayer->addChild(getSecretNumSprite);
    
    makeHomeBtn();
}

void GameClearScene::getAllSecretClear(float tm)
{
    //TODO:シークレット全て入手しているクリア演出
    makeHomeBtn();
}

void GameClearScene::makeHomeBtn()
{
    //ホームボタン
    _homeButton = Button::create();
    _homeButton->setTouchEnabled(true);
    _homeButton->loadTextures("Graphics/common/ok_off.png", "Graphics/common/ok_on.png", "");
    _homeButton->setPosition(Point(320, 400));
    _homeButton->addTouchEventListener(CC_CALLBACK_2(StageBaseScene::touchEvent, this));
    _stageLayer->addChild(_homeButton, HOME_BTN, TO_HOME_ACT);
}
