#ifndef __STAGE_01_H__
#define __STAGE_01_H__

#include "cocos2d.h"
#include "StageBaseScene.h"

class Stage01 : public StageBaseScene
{
public:
    static cocos2d::Scene* createScene();
    virtual bool init();
    CREATE_FUNC(Stage01);
private:
    enum LAP
    {
        STAGE_LAYER_0 = 0,
        ROOM_0,
        CLEAR_BTN,
        SECRET,
        SECRET_COVER,
        ROOM
    };
    enum SECRET_STATUS
    {
        BTN_HIDDEN = 0,
        CATCH_COVER,
        COVER_FALL,
        PUSHED_BTN,
        GET_SECRET
    };
    bool onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* event);
    void onTouchMoved(cocos2d::Touch* touch, cocos2d::Event* event);
    void onTouchEnded(cocos2d::Touch* touch, cocos2d::Event* event);
    void touchEvent(Ref* pSender, cocos2d::ui::Widget::TouchEventType type);
    virtual void update(float dt);
    void secretAct();
    void secretTouchEnabled();
    
    cocos2d::Layer* _stageLayer0;
    cocos2d::Sprite* _secret;
    cocos2d::Sprite* _secretBtn;
    cocos2d::Sprite* _secretCover;
    cocos2d::ui::Button* _secretButton;
    
    float _coverSpeedY;
    cocos2d::Point _catchCoverPoint;
//    cocos2d::Point _coverPosition;
    SECRET_STATUS _secretStatus;
};

#endif /* defined(__STAGE_01_H__) */
