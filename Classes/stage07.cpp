#include "stage07.h"

USING_NS_CC;
using namespace ui;

static const float OJI_POSITION_Y = 350.0;
static const Rect DAZZLING_RECT = Rect(0, 650, 300, 490);

Scene* Stage07::createScene()
{
    auto scene = Scene::create();
    
    auto layer = Stage07::create();
    layer->setTag(PARENT_LAYER_TAG);
    scene->addChild(layer);
    
    return scene;
}

bool Stage07::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
    setCommon(STAGE_07);
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Point origin = Director::getInstance()->getVisibleOrigin();
    
    _hangRect = Rect(77, -310 + visibleSize.height + origin.y, 80, 60);
    _ojiSpeedY = 0.0;
    _ojiStatus = OJI_SIT_DOWN;
    _lightStatus = LIGHT_ON;
    _secretStatus = SECRET_HIDDEN;
    
    //タッチ
    auto dispatcher = Director::getInstance()->getEventDispatcher();
    auto listener = EventListenerTouchOneByOne::create();
    listener->onTouchBegan = CC_CALLBACK_2(Stage07::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(Stage07::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(Stage07::onTouchEnded, this);
    dispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    
    //壁断面
    auto sectionSprite = Sprite::create("Graphics/07/room07_2.png");
    sectionSprite->setPosition(Point(322, 647));
    _stageLayer->addChild(sectionSprite, SECTION);
    //背景画像
    auto roomSprite = Sprite::create("Graphics/07/room07_0.png");
    roomSprite->setAnchorPoint(Point::ZERO);
    roomSprite->setPosition(Point::ZERO);
    _stageLayer->addChild(roomSprite, ROOM);
    //動く壁部分
    _moveWall = Sprite::create("Graphics/07/room07_1.png");
    _moveWall->setPosition(Point(320, 795));
    _stageLayer->addChild(_moveWall, WALL);
    //スイッチ
    _switch = Sprite::create("Graphics/07/switch07.png");
    _switch->setPosition(Point(580, 610));
    _stageLayer->addChild(_switch, OJISAN);
    //おじさん
    _oji = Sprite::create("Graphics/07/oji07.png");
    _oji->setPosition(Point(480, OJI_POSITION_Y));
    _stageLayer->addChild(_oji, OJISAN);
    //裸電球
    _light = Sprite::create("Graphics/07/light07_on.png");
    _light->setAnchorPoint(Point::ANCHOR_TOP_LEFT);
    _light->setPosition(Point(0, visibleSize.height + origin.y));
    _stageLayer->addChild(_light, LIGHT);
    //シークレット
    _secret = Sprite::create("Graphics/07/secret07.png");
    _secret->setPosition(Point(186, OJI_POSITION_Y));
    _stageLayer->addChild(_secret, SECRET);
    _secret->setVisible(false);
    //クリアボタン
    _clearButton = Button::create();
    _clearButton->setTouchEnabled(false);
    _clearButton->loadTextures("Graphics/common/clear_btn.png", "Graphics/common/clear_btn.png", "");
    _clearButton->setPosition(Point(320, 601));
    _clearButton->addTouchEventListener(CC_CALLBACK_2(StageBaseScene::touchEvent, this));
    _stageLayer->addChild(_clearButton, CLEAR_BTN, STAGE_CLEAR_ACT);
    //シークレットボタン
    _secretButton = Button::create();
    _secretButton->setTouchEnabled(false);
    _secretButton->loadTextures("Graphics/common/secret_btn.png", "Graphics/common/secret_btn.png", "");
    _secretButton->setPosition(Point(200, 320));
    _secretButton->addTouchEventListener(CC_CALLBACK_2(Stage07::touchEvent, this));
    _stageLayer->addChild(_secretButton, SECRET_BTN);
    
    scheduleUpdate();
    
    return true;
}

bool Stage07::onTouchBegan(Touch *touch, Event *event)
{
    Rect switchRect = _switch->boundingBox();
    Rect ojiRect = _oji->boundingBox();
    
    if(switchRect.containsPoint(touch->getLocation()))
    {
        switchLightStatus();
    }
    else if (ojiRect.containsPoint(touch->getLocation()))
    {
        if (_ojiStatus == OJI_SIT_DOWN)
        {
            setOjiStatus(OJI_CATCH);
            _ojiCatchPoint = _oji->getPosition() - touch->getLocation();
        }
    }
    
    return true;
}

void Stage07::onTouchMoved(Touch *touch, Event *event)
{
    if (_ojiStatus == OJI_CATCH)
    {
        Point touchPoint = touch->getLocation();
        Point ojiPosition = touchPoint + _ojiCatchPoint;
        
        Rect ojiRect = _oji->boundingBox();
        if(ojiRect.containsPoint(touchPoint))
        {
            if (ojiPosition.y > OJI_POSITION_Y)
            {
                setOjiStatus(OJI_PUT_UP);
                _ojiCatchPoint.x = _oji->getPosition().x - touchPoint.x;
            }
        }
        else
        {
            setOjiStatus(OJI_SIT_DOWN);
        }
    }
    else if (_ojiStatus == OJI_PUT_UP ||
             _ojiStatus == OJI_DAZZLING)
    {
        Point touchPoint = touch->getLocation();
        Point ojiPosition = touchPoint + _ojiCatchPoint;
        ojiPosition.x = MAX(40, MIN(ojiPosition.x, 600));
        ojiPosition.y = MAX(OJI_POSITION_Y, MIN(ojiPosition.y, 1060));
        
        if (_ojiStatus == OJI_PUT_UP)
        {
            _oji->setPosition(ojiPosition);
            
            if (_lightStatus == LIGHT_ON)
            {
                if (DAZZLING_RECT.containsPoint(ojiPosition))
                {
                    setOjiStatus(OJI_DAZZLING);
                }
            }
        }
        else if (_ojiStatus == OJI_DAZZLING)
        {
            _oji->setPosition(ojiPosition);
            
            if (!DAZZLING_RECT.containsPoint(ojiPosition))
            {
                setOjiStatus(OJI_PUT_UP);
            }
        }
    }
}

void Stage07::onTouchEnded(Touch *touch, Event *event)
{
    if (_ojiStatus == OJI_CATCH)
    {
        setOjiStatus(OJI_SIT_DOWN);
    }
    else if (_ojiStatus == OJI_PUT_UP ||
             _ojiStatus == OJI_DAZZLING)
    {
        setOjiStatus(OJI_FALL);
    }
}

void Stage07::update(float dt)
{
    if (_ojiStatus == OJI_FALL)
    {
        Point ojiPosition = _oji->getPosition();
        _ojiSpeedY += 120.0;
        ojiPosition.y -= _ojiSpeedY * dt;
        
        
        if(_lightStatus == LIGHT_OFF &&
           _hangRect.containsPoint(ojiPosition))
        {
            setOjiStatus(OJI_HANG);
        }
        else if (ojiPosition.y < OJI_POSITION_Y)
        {
            ojiPosition.y = OJI_POSITION_Y;
            setOjiStatus(OJI_SIT_DOWN);
            _ojiSpeedY = 0.0;
            
            _oji->setPosition(ojiPosition);
        }
        else
        {
            _oji->setPosition(ojiPosition);
        }
    }
}

void Stage07::setOjiStatus(Stage07::OJI_STATUS status)
{
    _ojiStatus = status;
    
    if (status == OJI_SIT_DOWN)
    {
        _oji->stopAllActions();
        _oji->setTexture("Graphics/07/oji07.png");
    }
    else if (status == OJI_PUT_UP)
    {
        _oji->stopAllActions();
        _oji->setTexture("Graphics/07/oji07.png");
    }
    else if (status == OJI_DAZZLING)
    {
        ojiAnimation();
    }
    else if (status == OJI_HANG)
    {
        Size visibleSize = Director::getInstance()->getVisibleSize();
        Point origin = Director::getInstance()->getVisibleOrigin();
        _oji->stopAllActions();
        _oji->setTexture("Graphics/07/oji07_hang.png");
        _oji->setPosition(Point(117, -360 + visibleSize.height + origin.y));
        _oji->runAction(EaseIn::create(MoveBy::create(0.8, Point(0, -80)), 2));
        _light->runAction(EaseIn::create(MoveBy::create(0.8, Point(0, -80)), 2));
        ActionInterval* moveAct = EaseIn::create(MoveBy::create(0.8, Point(0, 140)), 2);
        cocos2d::CallFunc *callback = CallFunc::create([this]()
                                                       {
                                                           btnEnabled(_clearButton, true);
                                                       });
        _moveWall->runAction(Sequence::create(moveAct, callback, nullptr));
    }
    else if (status == OJI_SECRET_JUMP)
    {
        _oji->stopAllActions();
        _light->stopAllActions();
        _moveWall->stopAllActions();
        //電気と壁を元の位置に
        Size visibleSize = Director::getInstance()->getVisibleSize();
        Point origin = Director::getInstance()->getVisibleOrigin();
        _light->runAction(EaseIn::create(MoveTo::create(0.8, Point(0, visibleSize.height + origin.y)), 2));
        _moveWall->runAction(EaseIn::create(MoveTo::create(0.8, Point(320, 795)), 2));
        _clearButton->setTouchEnabled(false);
        //シークレット出現前なら
        if (_secretStatus == SECRET_HIDDEN)
        {
            _secretStatus = SECRET_FALL;
            _secret->setPosition(_oji->getPosition());
            _secret->setVisible(true);
            ActionInterval* secretJumpAct = JumpTo::create(0.6, _secretButton->getPosition(), 100, 1);
            cocos2d::CallFunc *secretCallback = CallFunc::create([this]()
                                                                 {
                                                                     btnEnabled(_secretButton, true);
                                                                 });
            _secret->runAction(Sequence::create(secretJumpAct, secretCallback, nullptr));
        }
        //おじさん飛ぶ
        ojiAnimation();
        ActionInterval* ojiJumpAct = JumpTo::create(0.7, Point(450, OJI_POSITION_Y), 250, 1);
        cocos2d::CallFunc *ojiCallback = CallFunc::create([this]()
                                                          {
                                                              setOjiStatus(OJI_SIT_DOWN);
                                                          });
        _oji->runAction(Sequence::create(ojiJumpAct, ojiCallback, nullptr));
    }
}

void Stage07::switchLightStatus()
{
    if (_lightStatus == LIGHT_ON)
    {
        _lightStatus = LIGHT_OFF;
        _light->setTexture("Graphics/07/light07_off.png");
    }
    else if (_lightStatus == LIGHT_OFF)
    {
        _lightStatus = LIGHT_ON;
        _light->setTexture("Graphics/07/light07_on.png");
        
        if (_ojiStatus == OJI_HANG)
        {
            setOjiStatus(OJI_SECRET_JUMP);
        }
    }
}

void Stage07::ojiAnimation()
{
    auto animate = Animation::create();
    std::string ojiAnimeImg[6] =
    {
        "Graphics/07/oji07_0.png",
        "Graphics/07/oji07_1.png",
        "Graphics/07/oji07_2.png",
        "Graphics/07/oji07_3.png",
        "Graphics/07/oji07_4.png",
        "Graphics/07/oji07_5.png"
    };
    for (int i = 0; i < 6; i++)
    {
        animate->addSpriteFrameWithFile(ojiAnimeImg[i]);
    }
    animate->setDelayPerUnit(0.08); //1コマの表示時間
    
    ActionInterval* ojiAnime = RepeatForever::create(Animate::create(animate));
    _oji->runAction(ojiAnime);
}

void Stage07::btnEnabled(Button* btn, bool enable)
{
    //アニメーション後にボタン切り替えるためのメソッド
    btn->setTouchEnabled(enable);
}

void Stage07::touchEvent(Ref *pSender, Widget::TouchEventType type)
{
    if (type == Widget::TouchEventType::ENDED)
    {
        //シークレット入手
        _secret->removeFromParentAndCleanup(true);
        _secretButton->removeFromParentAndCleanup(true);
        getSecret();
    }
}
