#include "HomeTopLayer.h"
#include "HomeScene.h"

USING_NS_CC;
using namespace ui;

bool HomeTopLayer::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    return true;
}

void HomeTopLayer::setBackgroundImg(int stage)
{
    Sprite* bgSprite;
    
    if (!stage)
    {
        bgSprite = Sprite::create("Graphics/top/room_top_first.png");
    }
    else
    {
        bgSprite = Sprite::create("Graphics/top/room_top.png");
    }
    
    bgSprite->setAnchorPoint(Point::ZERO);
    bgSprite->setPosition(Point::ZERO);
    
    this->addChild(bgSprite, 0);
}
