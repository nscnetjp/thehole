#include "SecretWatchLayer.h"
#include "StageBaseScene.h"

USING_NS_CC;
using namespace ui;

bool SecretWatchLayer::init()
{
    if ( !LayerColor::initWithColor(Color4B(0, 0, 0, 0)) )
    {
        return false;
    }
    auto dispatcher = Director::getInstance()->getEventDispatcher();
    auto listener = EventListenerTouchOneByOne::create();
    listener->onTouchBegan = CC_CALLBACK_2(SecretWatchLayer::onTouchBegan, this);
    listener->setSwallowTouches(true);
    dispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    
    cocos2d::CallFunc *callback = CallFunc::create([this]()
                                                   {
                                                       startAction();
                                                   });
    this->runAction(Sequence::create(FadeTo::create(0.2, 160), callback, NULL));
    
    return true;
}

void SecretWatchLayer::setStageNo(STAGE stage)
{
    _secretNo = stage;
}

bool SecretWatchLayer::onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *unused_event)
{
    return true;
}

void SecretWatchLayer::startAction()
{
    std::string secretImgName = StringUtils::format("Graphics/sp/oji_s%02d.png", _secretNo + 1);
    _secretSprite = Sprite::create(secretImgName);
    _secretSprite->setPosition(Point(320, 1260));
    addChild(_secretSprite);
    ActionInterval* moveAct = EaseOut::create(MoveBy::create(0.5, Point(0, -670)), 2.5);
    cocos2d::CallFunc *callback = CallFunc::create([this]()
                                                   {
                                                       makeCloseBtn();
                                                   });
    _secretSprite->runAction(Sequence::create(moveAct, callback, NULL));
}

void SecretWatchLayer::makeCloseBtn()
{
    //閉じるボタン
    _closeButton = Button::create();
    _closeButton->setTouchEnabled(true);
    _closeButton->loadTextures("Graphics/sp/close_btn_off.png", "Graphics/sp/close_btn_on.png", "");
    _closeButton->setPosition(Point(475, 765));
    _closeButton->addTouchEventListener(CC_CALLBACK_2(SecretWatchLayer::touchEvent, this));
    this->addChild(_closeButton, CLOSE);
}

void SecretWatchLayer::touchEvent(Ref *pSender, Widget::TouchEventType type)
{
    if (type == Widget::TouchEventType::ENDED)
    {
        _secretSprite->removeFromParentAndCleanup(true);
        _closeButton->removeFromParentAndCleanup(true);
        ActionInterval* fadeAction = FadeOut::create(0.4);
        cocos2d::CallFunc *callback = CallFunc::create([this]()
                                                       {
                                                           endAction();
                                                       });
        this->runAction(Sequence::create(fadeAction, callback, NULL));
    }
}

void SecretWatchLayer::endAction()
{
    Scene *myScene = Director::getInstance()->getRunningScene();
    StageBaseScene *parentLayer = (StageBaseScene*)myScene->getChildByTag(PARENT_LAYER_TAG);
    parentLayer->gatSecret();
    
    this->removeFromParentAndCleanup(true);
}
