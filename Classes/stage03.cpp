#include "stage03.h"

USING_NS_CC;
using namespace ui;

static const int SECRET_COUNT = 20;

Scene* Stage03::createScene()
{
    auto scene = Scene::create();
    
    auto layer = Stage03::create();
    layer->setTag(PARENT_LAYER_TAG);
    scene->addChild(layer);
    
    return scene;
}

bool Stage03::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
    setCommon(STAGE_03);
    
    _holeHitCount = 0;
    _hitCount = 0;
    _hammerStatus = ITEM_FALL;
    
    //タッチ
    auto dispatcher = Director::getInstance()->getEventDispatcher();
    auto listener = EventListenerTouchOneByOne::create();
    listener->onTouchBegan = CC_CALLBACK_2(Stage03::onTouchBegan, this);
    dispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    //背景
    auto roomSprite = Sprite::create("Graphics/03/room03.png");
    roomSprite->setAnchorPoint(Point::ZERO);
    roomSprite->setPosition(Point::ZERO);
    _stageLayer->addChild(roomSprite, ROOM);
    //排気口
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Point origin = Director::getInstance()->getVisibleOrigin();
    auto exhaustPort = Sprite::create("Graphics/common/exhaustPort.png");
    exhaustPort->setPosition(Point(60, -140 + visibleSize.height + origin.y));
    _stageLayer->addChild(exhaustPort, ROOM);
    //穴
    _hole = Sprite::create("Graphics/03/hole03_0.png");
    _hole->setPosition(Point(320, 620));
    _stageLayer->addChild(_hole, HOLE_COVER);
    //ハンマー
    _hammer = Sprite::create("Graphics/03/hammer03.png");
    _hammer->setPosition(Point(460, 290));
    _stageLayer->addChild(_hammer, HOLE_COVER);
    //入手アイテム画像
    _item = Sprite::create("Graphics/03/hammer03_off.png");
    _item->setPosition(Point(_getItemPosition.x,_getItemPosition.y - 200));
    _stageLayer->addChild(_item, ITEM_BTN);
    _item->setVisible(false);
    //アイテムボタン
    _itemButton = Button::create();
    _itemButton->setTouchEnabled(false);
    _itemButton->loadTextures("Graphics/common/item_btn.png", "Graphics/common/item_btn.png", "");
    _itemButton->setPosition(_getItemPosition);
    _itemButton->addTouchEventListener(CC_CALLBACK_2(Stage03::touchEvent, this));
    _stageLayer->addChild(_itemButton, ITEM_BTN, ITEM_ACT);
    //ステージ番号書かれた額
    _picture[0] = Sprite::create("Graphics/03/picture03_0.png");
    _picture[0]->setAnchorPoint(Point(0.5, 0.8));
    _picture[0]->setPosition(Point(521, 608));
    _stageLayer->addChild(_picture[0], PICTURE);
    //猫の絵
    _picture[1] = Sprite::create("Graphics/03/picture03_1.png");
    _picture[1]->setAnchorPoint(Point(0.5, 0.8));
    _picture[1]->setPosition(Point(107, 682));
    _stageLayer->addChild(_picture[1], PICTURE);
    //裏にシークレットのある額
    _picture[2] = Sprite::create("Graphics/03/picture03_2.png");
    _picture[2]->setAnchorPoint(Point(0.5, 0.8));
    _picture[2]->setPosition(Point(432, 850));
    _stageLayer->addChild(_picture[2], PICTURE);
    //クリアボタン
    _clearButton = Button::create();
    _clearButton->setTouchEnabled(false);
    _clearButton->loadTextures("Graphics/common/clear_btn.png", "Graphics/common/clear_btn.png", "");
    _clearButton->setPosition(Point(320, 620));
    _clearButton->addTouchEventListener(CC_CALLBACK_2(StageBaseScene::touchEvent, this));
    _stageLayer->addChild(_clearButton, CLEAR_BTN, STAGE_CLEAR_ACT);
    //シークレットボタン
    _secretButton = Button::create();
    _secretButton->setTouchEnabled(false);
    _secretButton->loadTextures("Graphics/common/secret_btn.png", "Graphics/common/secret_btn.png", "");
    _secretButton->setPosition(Point(440, 300));
    _secretButton->addTouchEventListener(CC_CALLBACK_2(Stage03::touchEvent, this));
    _stageLayer->addChild(_secretButton, SECRET_BTN, SECRET_ACT);
    
    return true;
}

bool Stage03::onTouchBegan(Touch *touch, Event *unused_event)
{
    Point touchPoint = touch->getLocation();
    
    if (_hammerStatus == ITEM_FALL)
    {
        Rect hammerRect = _hammer->boundingBox();
        
        if(hammerRect.containsPoint(touchPoint))
        {
            _hammer->removeFromParentAndCleanup(true);
            _hammerStatus = ITEM_UNUSE;
            
            _item->setVisible(true);
            _item->runAction(EaseOut::create(JumpTo::create(0.2, _getItemPosition, 100, 1), 2));
            _itemButton->setTouchEnabled(true);
        }
    }
    else if (_hammerStatus == ITEM_USE)
    {
        Rect holeRect = _hole->boundingBox();
        
        if(holeRect.containsPoint(touchPoint)) //穴開く場所
        {
            shakeMove();
            
            _holeHitCount++;
            
            if (_holeHitCount == 2) //ヒビが大きく
            {
                _hole->setTexture("Graphics/03/hole03_1.png");
            }
            else if (_holeHitCount == 5) //穴があく
            {
                _hole->setTexture("Graphics/03/hole03_2.png");
                scheduleOnce(schedule_selector(Stage03::holeInEnabled), 0.5); //クリアボタンが押せるまで
            }
        }
        else if (touchPoint.y > 370) //穴開く場所以外の壁
        {
            shakeMove();
        }
        
    }
    
    return true;
}

void Stage03::touchEvent(Ref *pSender, Widget::TouchEventType type)
{
    Node *node = dynamic_cast<Node *>(pSender);
    int buttonTag = node->getTag();
    
    //アイテムボタンの切り替え
    if (buttonTag == ITEM_ACT)
    {
        switch (type)
        {
            case Widget::TouchEventType::BEGAN:
                if (_hammerStatus == ITEM_UNUSE)
                {
                    _item->setTexture("Graphics/03/hammer03_on.png");
                }
                break;
                
            case Widget::TouchEventType::ENDED:
                if (_hammerStatus == ITEM_USE)
                {
                    _hammerStatus = ITEM_UNUSE;
                    _item->setTexture("Graphics/03/hammer03_off.png");
                }
                else if (_hammerStatus == ITEM_UNUSE)
                {
                    _hammerStatus = ITEM_USE;
                }
                break;
                
            case Widget::TouchEventType::CANCELED:
                if (_hammerStatus == ITEM_UNUSE)
                {
                    _hammerStatus = ITEM_USE;
                }
                break;
                
            default:
                break;
        }
    }
    //シークレットボタン
    else if (buttonTag == SECRET_ACT)
    {
        if (type == Widget::TouchEventType::ENDED)
        {
            _picture[2]->setTexture("Graphics/03/picture03_4.png");
            getSecret();
            _secretButton->removeFromParentAndCleanup(true);
        }
    }
}

void Stage03::shakeMove()
{
    _hitCount++;
    
    ActionInterval* startScale = ScaleTo::create(0.02f, 1.05);
    Spawn* startSpawn;
    ActionInterval* upMove = MoveTo::create(0.02f, Point(0, 20));
    ActionInterval* downMove = MoveTo::create(0.02f, Point(0, -20));
    ActionInterval* rightMove = MoveTo::create(0.02f, Point(20, 0));
    ActionInterval* lefttMove = MoveTo::create(0.02f, Point(-20, 0));
    ActionInterval* endMove = MoveTo::create(0.02f, Point::ZERO);
    ActionInterval* endScale = ScaleTo::create(0.02f, 1.0);
    Spawn* endSpawn = Spawn::create(endMove, endScale, nullptr);
    
    int shakePattern = rand() % 5;
    
    switch (shakePattern)
    {
        case 0:
             startSpawn = Spawn::create(rightMove, startScale, nullptr);
            _stageLayer->runAction(Sequence::create(startSpawn, downMove, upMove, lefttMove, endSpawn, nullptr));
            break;
            
        case 1:
            startSpawn = Spawn::create(lefttMove, startScale, nullptr);
            _stageLayer->runAction(Sequence::create(startSpawn, rightMove, upMove, downMove, endSpawn, nullptr));
            break;
            
        case 2:
            startSpawn = Spawn::create(downMove, startScale, nullptr);
            _stageLayer->runAction(Sequence::create(startSpawn, upMove, lefttMove, rightMove, endSpawn, nullptr));
            break;
            
        case 3:
            startSpawn = Spawn::create(upMove, startScale, nullptr);
            _stageLayer->runAction(Sequence::create(startSpawn, rightMove, lefttMove, downMove, endSpawn, nullptr));
            break;
            
        case 4:
            startSpawn = Spawn::create(upMove, startScale, nullptr);
            _stageLayer->runAction(Sequence::create(startSpawn, lefttMove, rightMove, downMove, endSpawn, nullptr));
            break;
            
        default:
            break;
    }
    
    this->scheduleOnce(schedule_selector(Stage03::leanFlame), 0.05);
}

void Stage03::leanFlame(float tm)
{
    int flameCount;
    
    if (_hitCount < SECRET_COUNT)
    {
        flameCount = 3;
    }
    else if (_hitCount == SECRET_COUNT)
    {
        flameCount = 2;
        ActionInterval* moveAct = EaseIn::create(MoveBy::create(0.4, Point(0, -450)), 2.0);
        cocos2d::CallFunc *callback = CallFunc::create([this]()
                                                       {
                                                           fellFlame();
                                                       });
        _picture[2]->runAction(Sequence::create(moveAct, callback, nullptr));
    }
    else
    {
        flameCount = 2;
    }
    
    float rotateAngle[8] =
    {
        0,
        0,
        0,
        0,
        3,
        6,
        -3,
        -6
    };
    
    for (int i = 0; i < flameCount; ++i)
    {
        ActionInterval* jump = JumpBy::create(0.04, Point::ZERO, 30, 1);
        int rotatePattern = rand() % 8;
        ActionInterval* rotate = RotateTo::create(0.04, rotateAngle[rotatePattern]);
        _picture[i]->runAction(Spawn::create(jump, rotate, nullptr));
    }
}

void Stage03::fellFlame()
{
    _picture[2]->setTexture("Graphics/03/picture03_3.png");
    _picture[2]->setRotation(0.0);
    _picture[2]->setAnchorPoint(Point::ANCHOR_MIDDLE_TOP);
    _secretButton->setTouchEnabled(true);
}

void Stage03::holeInEnabled(float flame)
{
    _clearButton->setTouchEnabled(true);
}
