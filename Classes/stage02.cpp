#include "stage02.h"

USING_NS_CC;
using namespace ui;

static const Point HOLE_COVER_POSITION = Point(360, 590);

static const float COVER_MIN_X = 220;
static const float COVER_MAX_X = 360;

Scene* Stage02::createScene()
{
    auto scene = Scene::create();
    
    auto layer = Stage02::create();
    layer->setTag(PARENT_LAYER_TAG);
    scene->addChild(layer);
    
    return scene;
}

bool Stage02::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
    setCommon(STAGE_02);
    
    _coverPositionX = 0.0;
    _touchBeganPointX = 0.0;
    _coverMoveFlag = false;
    _secretFlag = false;
    _btnStatus = SECRET_HIDDEN;
    
    //タッチ
    auto dispatcher = Director::getInstance()->getEventDispatcher();
    auto listener = EventListenerTouchOneByOne::create();
    listener->onTouchBegan = CC_CALLBACK_2(Stage02::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(Stage02::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(Stage02::onTouchEnded, this);
    dispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    
    //背景
    auto roomSprite = Sprite::create("Graphics/02/room02.png");
    roomSprite->setAnchorPoint(Point::ZERO);
    roomSprite->setPosition(Point::ZERO);
    _stageLayer->addChild(roomSprite, ROOM);
    //排気口
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Point origin = Director::getInstance()->getVisibleOrigin();
    auto exhaustPort = Sprite::create("Graphics/common/exhaustPort.png");
    exhaustPort->setPosition(Point(60, -140 + visibleSize.height + origin.y));
    _stageLayer->addChild(exhaustPort, ROOM);
    //穴を覆っているもの
    _holeCover = Sprite::create("Graphics/02/cover02.png");
    _holeCover->setPosition(HOLE_COVER_POSITION);
    _stageLayer->addChild(_holeCover, HOLE_COVER);
    //シークレット画像
    _secret = Sprite::create("Graphics/02/secret02.png");
    _secret->setPosition(Point(71, 763));
    _stageLayer->addChild(_secret, SECRET);
    //シークレットを覆っているもの
    _secretCover = Sprite::create("Graphics/02/secret02_cover_0.png");
    _secretCover->setAnchorPoint(Point(0.05, 0.05));
    _secretCover->setPosition(Point(18,708));
    _stageLayer->addChild(_secretCover, SECRET_COVER);
    //クリアボタン
    _clearButton = Button::create();
    _clearButton->setTouchEnabled(false);
    _clearButton->loadTextures("Graphics/common/clear_btn.png", "Graphics/common/clear_btn.png", "");
    _clearButton->setPosition(Point(360, 590));
    _clearButton->addTouchEventListener(CC_CALLBACK_2(StageBaseScene::touchEvent, this));
    _stageLayer->addChild(_clearButton, CLEAR_BTN, STAGE_CLEAR_ACT);
    //シークレットボタン
    Button* secretButton = Button::create();
    secretButton->setTouchEnabled(true);
    secretButton->loadTextures("Graphics/common/secret_btn.png", "Graphics/common/secret_btn.png", "");
    secretButton->setPosition(Point(71, 763));
    secretButton->addTouchEventListener(CC_CALLBACK_2(Stage02::touchEvent, this));
    _stageLayer->addChild(secretButton, SECRET_BTN);
    
    return true;
}

bool Stage02::onTouchBegan(Touch *touch, Event *event)
{
    Point point = touch->getLocation();
    Rect  rect = _holeCover->boundingBox();
    
    if(rect.containsPoint(point))
    {
        _coverPositionX = _holeCover->getPosition().x;
        _touchBeganPointX = touch->getLocation().x;
        _coverMoveFlag = true;
    }
        
    return true;
}

void Stage02::onTouchMoved(Touch *touch, Event *unused_event)
{
    if (_coverMoveFlag)
    {
        if (fabs(touch->getLocation().y - HOLE_COVER_POSITION.y) > 110)
        {
            _coverMoveFlag = false;
        }
        
        float coverX = _coverPositionX + (touch->getLocation().x - _touchBeganPointX) / 5;
        coverX = MAX(COVER_MIN_X, MIN(coverX, COVER_MAX_X));
        _holeCover->setPosition(Point(coverX, HOLE_COVER_POSITION.y));
        
        if (coverX < 240)
        {
            _clearButton->setTouchEnabled(true);
        }
        else
        {
            _clearButton->setTouchEnabled(false);
        }
    }
}

void Stage02::onTouchEnded(Touch *touch, Event *event)
{
    _coverMoveFlag = false;
}

void Stage02::touchEvent(Ref *pSender, Widget::TouchEventType type)
{
    if (type == Widget::TouchEventType::ENDED)
    {
        if (_btnStatus == SECRET_HIDDEN)
        {
            ActionInterval* rotate1 = EaseInOut::create(RotateBy::create(0.5, 158), 2);
            ActionInterval* rotate2 = EaseInOut::create(RotateBy::create(0.2, -25), 2);
            _btnStatus = SHOW_SECRET;
            _secretCover->runAction(Sequence::create(rotate1, rotate2, nullptr));
            _secretCover->setTexture("Graphics/02/secret02_cover_1.png");
        }
        else if (_btnStatus == SHOW_SECRET)
        {
            Node *node = dynamic_cast<Node *>(pSender);
            node->removeFromParentAndCleanup(true);
            _secret->removeFromParentAndCleanup(true);
            getSecret();
        }
    }
}
