#include "StageBaseScene.h"
#include "GetSecretLayer.h"
#include "SecretWatchLayer.h"

USING_NS_CC;
using namespace ui;

static const float FADE_IN_DURATION = 0.2f;
static const float FADE_OUT_DURATION = 0.4f;
static const float CLEAR_DURATION = 0.5f;

void StageBaseScene::setCommon(STAGE stageNo)
{
    _repraceFlag = true;
    _stageNo = stageNo;
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Point origin = Director::getInstance()->getVisibleOrigin();
    
    //ステージレイヤー（このレイヤーにゲーム画面を作る）
    _stageLayer = Layer::create();
    _stageLayer->setPosition(Point(origin.x, 0));
    addChild(_stageLayer,STAGE_LAYER);
    
    //ヘッダー背景
    auto headerSprite = Sprite::create("Graphics/common/header.png");
    headerSprite->setAnchorPoint(Point::ANCHOR_TOP_LEFT);
    headerSprite->setPosition(Point(origin.x, visibleSize.height + origin.y));
    addChild(headerSprite,HEADER);
    
    //ヘッダーボタン(HOME)
    Button* homeButton = Button::create();
    homeButton->setTouchEnabled(true);
    homeButton->loadTextures("Graphics/common/home_off.png", "Graphics/common/home_on.png", "");
    homeButton->setAnchorPoint(Point::ANCHOR_TOP_LEFT);
    homeButton->setPosition(Point(10 + origin.x, visibleSize.height + origin.y));
    homeButton->addTouchEventListener(CC_CALLBACK_2(StageBaseScene::touchEvent, this));
    this->addChild(homeButton, HEADER_BTN, TO_HOME_ACT);
    
    //ヘッダーボタン（RETRY）
    Button* retryButton = Button::create();
    retryButton->setTouchEnabled(true);
    retryButton->loadTextures("Graphics/common/retry_off.png", "Graphics/common/retry_on.png", "");
    retryButton->setAnchorPoint(Point::ANCHOR_TOP_LEFT);
    retryButton->setPosition(Point(180 + origin.x, visibleSize.height + origin.y));
    retryButton->addTouchEventListener(CC_CALLBACK_2(StageBaseScene::touchEvent, this));
    this->addChild(retryButton, HEADER_BTN, RETRY_ACT);
    
    //ヘッダーボタン（シークレット）
    makeSecretBtn();
    
    //フッター
    auto footerSprite = Sprite::create("Graphics/common/footer.png");
    footerSprite->setAnchorPoint(Point::ZERO);
    footerSprite->setPosition(Point(origin.x, origin.y));
    addChild(footerSprite,FOOTER);
    
    //入手したアイテムが表示される位置
    _getItemPosition = Point(120, 190 + origin.y);
    
    //TODO:バナー広告を表示
    
    //開始時の演出
    startDirect();
}

void StageBaseScene::startDirect()
{
    _stageLayer->setScale(1.05f);
    _stageLayer->runAction(ScaleTo::create(FADE_IN_DURATION, 1.0f));
    //遷移するときの暗転用の画像
    _shadeSprite = Sprite::create("Graphics/common/black.png");
    _shadeSprite->setAnchorPoint(Point::ZERO);
    _shadeSprite->setPosition(Point::ZERO);
    addChild(_shadeSprite, SHADE);
    _shadeSprite->runAction(FadeOut::create(FADE_IN_DURATION));
}

void StageBaseScene::touchEvent(Ref *pSender, Widget::TouchEventType type)
{
    if (type == Widget::TouchEventType::ENDED)
    {
        Node *node = dynamic_cast<Node *>(pSender);
        int buttonTag = node->getTag();
        
        Point btnPosition = node->getPosition();
        
        switch (buttonTag)
        {
            case TO_HOME_ACT:
                repraceHome();
                break;
                
            case RETRY_ACT:
                retryStage();
                break;
                
            case STAGE_CLEAR_ACT:
                repraceNextStage(btnPosition);
                break;
                
            case STAGE_SECRET:
                watchSecret();
                break;
                
            default:
                break;
        }
    }
    
}

void StageBaseScene::repraceHome()
{
    if (_repraceFlag)
    {
        _repraceFlag = false;
        
        //暗転してホームへ遷移
        ActionInterval* FadeInAct = FadeIn::create(FADE_OUT_DURATION);
        CallFunc *callback = CallFunc::create([this]()
                                              {
                                                  Director::getInstance()->replaceScene(HomeScene::createScene());
                                              });
        _shadeSprite->runAction(Sequence::create(FadeInAct, callback, nullptr));
    }
}

void StageBaseScene::retryStage()
{
    if (_repraceFlag)
    {
        _repraceFlag = false;
        
        //暗転して現行ステージへ遷移
        cocos2d::CallFunc *callback = CallFunc::create([this]()
                                                       {
                                                           ReplaceStageClass::replaceStage(_stageNo);
                                                       });
        _shadeSprite->runAction(Sequence::create(FadeIn::create(FADE_OUT_DURATION), callback, nullptr));
    }
}

void StageBaseScene::repraceNextStage(Point holePosition)
{
    if (_repraceFlag)
    {
        _repraceFlag = false;
        
        //拡大
        _stageLayer->runAction(ScaleBy::create(CLEAR_DURATION + 0.05f, 1.3f));
        //黒丸画像
        auto holeInSprite = Sprite::create("Graphics/common/holein.png");
        holeInSprite->setPosition(holePosition);
        addChild(holeInSprite, SHADE);
        holeInSprite->runAction(ScaleBy::create(CLEAR_DURATION + 0.05f, 5.0f));
        //暗転して次のステージへ遷移
        cocos2d::CallFunc *callback = CallFunc::create([this]()
                                                       {
                                                           ReplaceStageClass::replaceStage(_stageNo + 1);
                                                       });
        _shadeSprite->runAction(Sequence::create(FadeIn::create(CLEAR_DURATION), callback, nullptr));
    }
}

void StageBaseScene::watchSecret()
{
    _stageSecret->runAction(EaseIn::create(MoveBy::create(0.2, Point(0, 80)), 2));
    auto secretLayer = SecretWatchLayer::create();
    secretLayer->setStageNo(_stageNo);
    this->addChild(secretLayer, SECRET_LAYER);
}

void StageBaseScene::makeSecretBtn()
{
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Point origin = Director::getInstance()->getVisibleOrigin();
    
    Point stageSecretPosition = Point(580 + origin.x, - 80 + visibleSize.height + origin.y);
    
    //ヘッダーボタン（シークレット）
    _stageSecret  = Button::create();
    _stageSecret->setTouchEnabled(true);
    _stageSecret->loadTextures("Graphics/sp/stageSecret.png", "Graphics/sp/stageSecret.png", "");
    _stageSecret->setAnchorPoint(Point::ANCHOR_MIDDLE_BOTTOM);
    _stageSecret->setPosition(stageSecretPosition);
    _stageSecret->addTouchEventListener(CC_CALLBACK_2(StageBaseScene::touchEvent, this));
    this->addChild(_stageSecret, HEADER_BTN, STAGE_SECRET);
    //セーーブデータを確認
    cocos2d::UserDefault* user = cocos2d::UserDefault::getInstance();
    std::string secretKey = StringUtils::format("secretKey_%d", _stageNo);
    long len = secretKey.length();
    char* keyName = new char[len+1];
    memcpy(keyName, secretKey.c_str(), len+1);
    bool secret = user->getBoolForKey(keyName, false);
    _stageSecret->setVisible(secret);
    
    _stageSecret->setScale(0.7, 0.3);
    ActionInterval* moveAct1 = EaseInOut::create(MoveBy::create(0.2, Point(0, 20)), 2);
    ActionInterval* scaleAct1 = EaseInOut::create(ScaleTo::create(0.2, 0.9, 1.1), 2);
    ActionInterval* spawn1 = Spawn::create(moveAct1, scaleAct1, nullptr);
    ActionInterval* moveAct2 = EaseIn::create(MoveTo::create(0.2, stageSecretPosition), 2);
    ActionInterval* scaleAct2 = EaseIn::create(ScaleTo::create(0.2, 1.0), 2);
    ActionInterval* spawn2 = Spawn::create(moveAct2, scaleAct2, nullptr);
    _stageSecret->runAction(Sequence::create(spawn1, spawn2, nullptr));
}

void StageBaseScene::getSecret()
{
    auto secretLayer = GetSecretLayer::create();
    this->addChild(secretLayer, SECRET_LAYER);
    secretLayer->saveStageNo(_stageNo);
}

void StageBaseScene::gatSecret()
{
    if (_stageNo != GAME_CLEAR)
    {
        _stageSecret->removeFromParentAndCleanup(true);
        makeSecretBtn();
    }
}
