#ifndef __MyCppGame__HomeTopLayer__
#define __MyCppGame__HomeTopLayer__

#include "cocos2d.h"
#include <ui/UIButton.h>

class HomeTopLayer : public cocos2d::Layer
{
public:
    virtual bool init();
    CREATE_FUNC(HomeTopLayer);
    
    void setBackgroundImg(int stage);
};

#endif /* defined(__MyCppGame__HomeTopLayer__) */
