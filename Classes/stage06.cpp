#include "stage06.h"

USING_NS_CC;
using namespace ui;

static const float PAINT_POSITION_MIN = 0;
static const float PAINT_POSITION_MAX = 375;
static const float OJI_ACC_BIG = 1;
static const float OJI_ACC_MIDDLE = 0.1;
static const float SECRET_ARM_SPEED = 3.0;

Scene* Stage06::createScene()
{
    auto scene = Scene::create();
    
    auto layer = Stage06::create();
    layer->setTag(PARENT_LAYER_TAG);
    scene->addChild(layer);
    
    return scene;
}

bool Stage06::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
    setCommon(STAGE_06);
    
    _ojiPosition = Point(-150, 651);
    _touchMoveTime = 0.0;
    _accX = 0.0;
    _ojiSpeedX = 0.0;
    _paintStatus = STOP;
    _ojiStatus = HUNG;
    
    //加速度
    Device::setAccelerometerEnabled(true);
    auto accelerationListener = EventListenerAcceleration::create(CC_CALLBACK_2(Stage06::onAcceleration, this));
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(accelerationListener, this);
    //タッチ
    auto dispatcher = Director::getInstance()->getEventDispatcher();
    auto listener = EventListenerTouchOneByOne::create();
    listener->onTouchBegan = CC_CALLBACK_2(Stage06::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(Stage06::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(Stage06::onTouchEnded, this);
    dispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    
    //背景
    auto roomSprite = Sprite::create("Graphics/06/room06.png");
    roomSprite->setAnchorPoint(Point::ZERO);
    roomSprite->setPosition(Point::ZERO);
    _stageLayer->addChild(roomSprite, ROOM);
    //穴あいたときの背景
    _roomBack = Sprite::create("Graphics/06/room06_0.png");
    _roomBack->setAnchorPoint(Point::ZERO);
    _roomBack->setPosition(Point::ZERO);
    _stageLayer->addChild(_roomBack, ROOM);
    _roomBack->setVisible(false);
    //ペンキ
    _paint0 = Sprite::create("Graphics/06/paint06_0.png");
    _paint0->setPosition(Point(PAINT_POSITION_MIN, 310));
    _stageLayer->addChild(_paint0, PAINT);
    //ペンキ
    _paint1 = Sprite::create("Graphics/06/paint06_1.png");
    _paint1->setPosition(Point(90, 310));
    _stageLayer->addChild(_paint1, PAINT);
    //穴があいたときに手前に来る床
    _roomFront = Sprite::create("Graphics/06/room06_1.png");
    _roomFront->setAnchorPoint(Point::ZERO);
    _roomFront->setPosition(Point::ZERO);
    _stageLayer->addChild(_roomFront, ROOM_FRONT);
    _roomFront->setVisible(false);
    //おじさん
    _oji = Sprite::create("Graphics/06/oji06_0.png");
    _oji->setPosition(_ojiPosition);
    _stageLayer->addChild(_oji, OJISAN);
    //金属バー
    auto barSprite = Sprite::create("Graphics/06/bar06.png");
    barSprite->setPosition(Point(320, 870));
    _stageLayer->addChild(barSprite, BAR);
    
    //クリアボタン
    _clearButton = Button::create();
    _clearButton->setTouchEnabled(false);
    _clearButton->loadTextures("Graphics/common/clear_btn.png", "Graphics/common/clear_btn.png", "");
    _clearButton->setPosition(Point(320, 260));
    _clearButton->addTouchEventListener(CC_CALLBACK_2(StageBaseScene::touchEvent, this));
    _stageLayer->addChild(_clearButton, CLEAR_BTN, STAGE_CLEAR_ACT);
    //シークレットボタン
    _secretButton = Button::create();
    _secretButton->setTouchEnabled(false);
    _secretButton->loadTextures("Graphics/common/secret_btn.png", "Graphics/common/secret_btn.png", "");
    _secretButton->setPosition(Point(270, 300));
    _secretButton->addTouchEventListener(CC_CALLBACK_2(Stage06::touchEvent, this));
    _stageLayer->addChild(_secretButton, SECRET_BTN);
    
    scheduleUpdate();
    
    return true;
}

bool Stage06::onTouchBegan(Touch *touch, Event *event)
{
    if (_ojiStatus == HUNG)
    {
        if (fabs(_oji->getPosition().x -320) < 20) //中心付近
        {
            Rect ojiRect = _oji->boundingBox();
            if(ojiRect.containsPoint(touch->getLocation()))
            {
                _ojiSpeedX = 0.0;
                _touchBeganPoint = touch->getLocation();
                _ojiStatus = OJI_TOUCH_BEGAN;
            }
        }
    }
    return true;
}

void Stage06::onTouchMoved(Touch *touch, Event *event)
{
    if (_ojiStatus == OJI_TOUCH_BEGAN)
    {
        _ojiStatus = OJI_TOUCH_MOVE;
    }
}

void Stage06::onTouchEnded(Touch *touch, Event *event)
{
    if (_ojiStatus == OJI_TOUCH_BEGAN)
    {
        _ojiStatus = HUNG;
    }
    if (_ojiStatus == OJI_TOUCH_MOVE)
    {
        if (touch->getLocation().y - _touchBeganPoint.y < - 200)
        {
            ojiFall();
        }
        else
        {
            _ojiStatus = HUNG;
        }
    }
}

void Stage06::onAcceleration(Acceleration *acc, Event *unused_event)
{
    _accX = acc->x;
    
    //加速度センサー使うヒント用のペンキ
    if (_paintStatus != END)
    {
        if (_accX < 0.2)
        {
            _paintStatus = STOP;
        }
        else
        {
            _paintStatus = RUN;
        }
    }
    
    if (_ojiStatus == HUNG)
    {
        if (fabs(_ojiSpeedX) < 0.1 && //スピードが出てない
            fabs(_oji->getPosition().x -320) < 20 && //中心付近
            acc->y < -1.7) //下方向に大きな加速度
        {
            ojiFall();
        }
    }
    else if (_ojiStatus == FELL ||
             _ojiStatus == RETURN)
    {
        if (acc->y < 0.0) _ojiStatus = FELL;
        else if (acc->y > 0.4) _ojiStatus = RETURN;
    }
}

void Stage06::update(float dt)
{
    if (_paintStatus == RUN)
    {
        Point paintPosition = _paint0->getPosition();
        paintPosition.x += 75 * dt;
        if (paintPosition.x > PAINT_POSITION_MAX)
        {
            paintPosition.x = PAINT_POSITION_MAX;
            _paintStatus = END;
        }
        _paint0->setPosition(paintPosition);
    }
    
    switch (_ojiStatus)
    {
        case HUNG:
            if (fabs(_accX) > 0.8) //かなり端末が傾いてる
            {
                _ojiSpeedX = _ojiSpeedX + _accX * OJI_ACC_BIG * dt;
                _ojiSpeedX = MIN(20, _ojiSpeedX);
            }
            else if (fabs(_accX) > 0.2) //わりと傾いてる
            {
                _ojiSpeedX = _ojiSpeedX + _accX * OJI_ACC_MIDDLE * dt;
                _ojiSpeedX = MIN(20, _ojiSpeedX);
            }
            
            if (fabs(_accX) <= 0.2 || //あまり傾いていない
                _ojiSpeedX * _accX < 0) //進行方向と逆に加速度がかかっている
            {
                if (fabs(_ojiSpeedX) > 5.0) _ojiSpeedX *= 0.99;
                else _ojiSpeedX *= 0.8;
            }
            
            _ojiPosition.x += _ojiSpeedX;
            if (_ojiPosition.x < -150) _ojiPosition.x = 790;
            if (_ojiPosition.x > 790) _ojiPosition.x = -150;
            _oji->setPosition(_ojiPosition);
            break;
            
        case OJI_TOUCH_MOVE:
            _touchMoveTime += dt;
            if (_touchMoveTime > 0.5)
            {
                _touchMoveTime = 0.0;
                _ojiStatus = HUNG;
            }
            break;
            
        case FELL:
            _ojiPosition.y = MAX(130, _ojiPosition.y - SECRET_ARM_SPEED);
            _oji->setPosition(_ojiPosition);
            if (_ojiPosition.y <= 190)
            {
                _secretButton->setTouchEnabled(false);
                _clearButton->setTouchEnabled(true);
            }
            break;
            
        case RETURN:
            _ojiPosition.y = MIN(265, _ojiPosition.y + SECRET_ARM_SPEED);
            _oji->setPosition(_ojiPosition);
            if (_ojiPosition.y > 190)
            {
                _secretButton->setTouchEnabled(true);
                _clearButton->setTouchEnabled(false);
            }
            break;
            
        default:
            break;
    }
}

void Stage06::makeHole(float tm)
{
    _roomBack->setVisible(true);
    _roomFront->setVisible(true);
    
    ActionInterval* startScale = ScaleTo::create(0.02f, 1.05);
    ActionInterval* endScale = ScaleTo::create(0.02f, 1.0);
    _stageLayer->runAction(Sequence::create(startScale, endScale, nullptr));
}

void Stage06::ojiFall()
{
    _ojiStatus = FALLING;
    
    this->scheduleOnce(schedule_selector(Stage06::makeHole), 0.4);
    
    ActionInterval* moveAct = EaseIn::create(MoveBy::create(0.8, Point(0, -800)), 2);
    ActionInterval* delayAct = DelayTime::create(0.3);
    cocos2d::CallFunc *callback = CallFunc::create([this]()
                                                   {
                                                       ojiFell();
                                                   });
    _oji->runAction(Sequence::create(moveAct, delayAct, callback, nullptr));
}

void Stage06::ojiFell()
{
    ActionInterval* upMove = MoveTo::create(0.02f, Point(0,20));
    ActionInterval* downMove = MoveTo::create(0.02f, Point(0,-20));
    ActionInterval* rightMove = MoveTo::create(0.02f, Point(20,0));
    ActionInterval* lefttMove = MoveTo::create(0.02f, Point(-20,0));
    ActionInterval* startScale = ScaleTo::create(0.02f, 1.02);
    Spawn* startSpawn = Spawn::create(upMove, startScale, nullptr);
    ActionInterval* endMove = MoveTo::create(0.02f, Point::ZERO);
    ActionInterval* endScale = ScaleTo::create(0.02f, 1.0);
    Spawn* endSpawn = Spawn::create(endMove, endScale, nullptr);
    cocos2d::CallFunc *callback = CallFunc::create([this]()
                                                   {
                                                       holeInEnabled();
                                                   });
    _stageLayer->runAction(Sequence::create(startSpawn, lefttMove, rightMove, downMove, upMove, endSpawn, callback, nullptr));
}

void Stage06::holeInEnabled()
{
    _ojiStatus = FELL;
    _clearButton->setTouchEnabled(true);
    _oji->setTexture("Graphics/06/oji06_2.png");
    _ojiPosition = Point(270, 130);
    _oji->setPosition(_ojiPosition);
}

void Stage06::touchEvent(Ref *pSender, Widget::TouchEventType type)
{
    if (type == Widget::TouchEventType::ENDED)
    {
        _secretButton->removeFromParentAndCleanup(true);
        _ojiStatus = LOSE_SECRET;
        
        _oji->setTexture("Graphics/06/oji06_3.png");
        ActionInterval* delay = DelayTime::create(1.6);
        ActionInterval* move = MoveBy::create(2.5, Point(0, -200));
        cocos2d::CallFunc* callback = CallFunc::create([this]()
                                                       {
                                                           afterGetSecret();
                                                       });
        _oji->runAction(Sequence::create(delay, move, callback, nullptr));
        
        getSecret();
    }
}

void Stage06::afterGetSecret()
{
    _clearButton->setTouchEnabled(true);
}
