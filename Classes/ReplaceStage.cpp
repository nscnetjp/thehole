#include "ReplaceStage.h"
#include "stage01.h"
#include "stage01.h"
#include "stage02.h"
#include "stage03.h"
#include "stage04.h"
#include "stage05.h"
#include "stage06.h"
#include "stage07.h"
#include "stage08.h"
#include "stage09.h"
#include "stage10.h"
#include "GameClearLayer.h"

USING_NS_CC;

void ReplaceStageClass::replaceStage(int stageNo)
{
    //ロード
    cocos2d::UserDefault* user = cocos2d::UserDefault::getInstance();
    int clearStage = user->getIntegerForKey("clearStageKey", 0);
    //セーブ
    if (stageNo > clearStage)
    {
        user->setIntegerForKey("clearStageKey", stageNo);
    }
    
    switch (stageNo)
    {        
        case STAGE_01:
            Director::getInstance()->replaceScene(Stage01::createScene());
            break;
            
        case STAGE_02:
            Director::getInstance()->replaceScene(Stage02::createScene());
            break;
            
        case STAGE_03:
            Director::getInstance()->replaceScene(Stage03::createScene());
            break;
            
        case STAGE_04:
            Director::getInstance()->replaceScene(Stage04::createScene());
            break;
            
        case STAGE_05:
            Director::getInstance()->replaceScene(Stage05::createScene());
            break;
            
        case STAGE_06:
            Director::getInstance()->replaceScene(Stage06::createScene());
            break;
            
        case STAGE_07:
            Director::getInstance()->replaceScene(Stage07::createScene());
            break;
            
        case STAGE_08:
            Director::getInstance()->replaceScene(Stage08::createScene());
            break;
            
        case STAGE_09:
            Director::getInstance()->replaceScene(Stage09::createScene());
            break;
            
        case STAGE_10:
            Director::getInstance()->replaceScene(Stage10::createScene());
            break;
        case GAME_CLEAR:
            Director::getInstance()->replaceScene(GameClearScene::createScene());
            break;
            
        default:
            break;
    }
}