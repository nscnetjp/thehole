#include "StageSelectBaseLayer.h"
#include "HomeScene.h"

USING_NS_CC;
using namespace ui;

static const float SCROLL_SPEED = 0.35;
static const float SCROLL_FRICTION = 0.99;
static const float FAST_SPEED = 900.0;
static const float HOLE_IN_DURATION = 0.5f;

bool StageSelectBaseLayer::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
    srand((unsigned int)time(NULL));
    
    _layerPosition = Point::ZERO;
    _touchPosition = 0.0;
    _touchMovedPosition = 0.0;
    _bgPosition = 0.0;
    _speed = 0.0;
    _btnTag = 0;
    _touchFlag = false;
    _btnFlag = true;
    
    auto shadeSprite = Sprite::create("Graphics/top/select_6.png");
    shadeSprite->setAnchorPoint(Point::ZERO);
    shadeSprite->setPosition(Point::ZERO);
    this->addChild(shadeSprite, SHADE);
    
    _stageSelectLayer = StageSelectLayer::create();
    addChild(_stageSelectLayer, BACKGROUND);
    
    return true;
}

//--------------------------------------------------------//
#pragma mark - ボタン作成とボタンを押したときの処理
//--------------------------------------------------------//

void StageSelectBaseLayer::setHole(int stage)
{
    Point holePosition[10]
    {
        Point(218, 400),
        Point(528, 706),
        Point(824, 268),
        Point(1008, 872),
        Point(1266, 432),
        Point(1624, 338),
        Point(1880, 800),
        Point(2068, 260),
        Point(2586, 364),
        Point(2732, 772)
    };
    
    //クリアしたステージに応じた穴の数だけ読み込む
    for (int i = 0; i < stage + 1; i++)
    {
        Button* startButton = Button::create();
        startButton->setTouchEnabled(true);
        
        std::string holeImgName = StringUtils::format("Graphics/top/hole%02d.png", i + 1);
        long len = holeImgName.length();
        char* fname = new char[len+1];
        memcpy(fname, holeImgName.c_str(), len+1);
        
        startButton->loadTextures(fname, fname, "");
        startButton->setPosition(holePosition[i]);
        startButton->addTouchEventListener(CC_CALLBACK_2(StageSelectBaseLayer::touchEvent, this));
        _stageSelectLayer->addChild(startButton, HOLE, STAGE_01 + i);
    }
}

void StageSelectBaseLayer::touchEvent(Ref *pSender, Widget::TouchEventType type)
{
    if (_btnFlag)
    {
        if (type == Widget::TouchEventType::BEGAN)
        {
            _speed = 0.0;
        }
        else if (type == Widget::TouchEventType::ENDED)
        {
            _btnFlag = false;
            
            unscheduleUpdate();
            
            Node *node = dynamic_cast<Node *>(pSender);
            _btnTag = node->getTag();
            
            //黒丸画像
            auto holeInSprite = Sprite::create("Graphics/common/holein.png");
            holeInSprite->setPosition(node->getPosition());
            _stageSelectLayer->addChild(holeInSprite, HOLE_IN);
            //遷移するときの暗転用の画像
            auto shadeSprite = Sprite::create("Graphics/common/black.png");
            shadeSprite->setAnchorPoint(Point::ZERO);
            shadeSprite->setPosition(Point::ZERO);
            this->addChild(shadeSprite, SHADE);
            shadeSprite->setOpacity(0); //フェードインさせるため透明に
            
            //アニメーションさせる(背景、穴、暗転)
            //拡大
            this->runAction(ScaleBy::create(HOLE_IN_DURATION + 0.05f, 1.3f));
            //穴が画面中心方向へ
            Point movePoint = Point(- node->getPosition().x + 320, 0);
            movePoint.x = MAX(-2360, MIN(movePoint.x, 0));
            _stageSelectLayer->runAction(MoveTo::create(HOLE_IN_DURATION + 0.05f, movePoint));
            //黒丸が大きく
            holeInSprite->runAction(ScaleBy::create(HOLE_IN_DURATION + 0.05f, 5.0f));
            //暗転して遷移
            ActionInterval* delayAct = DelayTime::create(0.1);
            ActionInterval* fadeAct = FadeIn::create(HOLE_IN_DURATION);
            CallFunc* callback = CallFunc::create([this]()
                                                  {
                                                      this->holeInReplace();
                                                  });
            shadeSprite->runAction(Sequence::create(delayAct, fadeAct, callback, nullptr));//この演出後遷移
        }
    }
}

void StageSelectBaseLayer::holeInReplace()
{
    ReplaceStageClass::replaceStage(_btnTag);
}

//--------------------------------------------------------//
#pragma mark - 左右にスクロールする動き
//--------------------------------------------------------//

void StageSelectBaseLayer::startScroll()
{
    auto dispatcher = Director::getInstance()->getEventDispatcher();
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    
    listener->onTouchBegan = CC_CALLBACK_2(StageSelectBaseLayer::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(StageSelectBaseLayer::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(StageSelectBaseLayer::onTouchEnded, this);
    
    dispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    
    scheduleUpdate();
}

bool StageSelectBaseLayer::onTouchBegan(Touch *touch, Event *event)
{
    _touchFlag = true;
    _touchPosition = touch->getLocation().x;
    _touchMovedPosition = _touchPosition;
    
    return true;
}

void StageSelectBaseLayer::onTouchMoved(Touch *touch, Event *event)
{
    _touchPosition = touch->getLocation().x;
}

void StageSelectBaseLayer::onTouchEnded(Touch *touch, Event *event)
{
    _touchFlag = false;
}

void StageSelectBaseLayer::update(float dt)
{
    if (_touchFlag)
    {
        float move_x = (_touchPosition - _touchMovedPosition) * SCROLL_SPEED;
        _speed = move_x / dt;
        _touchMovedPosition = _touchPosition;
        
        _layerPosition.x += move_x;
        _layerPosition.x = MAX(-2360, MIN(_layerPosition.x, 0));
        _stageSelectLayer->setPosition(_layerPosition);
    }
    else if (_speed != 0.0)
    {
        _speed *= SCROLL_FRICTION;
        float move_x = _speed * dt;
        
        if (fabs(move_x) > 1.0)
        {
            _layerPosition.x += move_x;
            _stageSelectLayer->setPosition(_layerPosition);
            
            if (_layerPosition.x < -2360 || _layerPosition.x > 0)
            {
                _layerPosition.x = MAX(-2360, MIN(_layerPosition.x, 0));
                _stageSelectLayer->setPosition(_layerPosition);
                
                if (fabs(_speed) > FAST_SPEED)
                {
                    ActionInterval* scaleAction1 = ScaleTo::create(0.04, 1.05);
                    ActionInterval* scaleAction2 = ScaleTo::create(0.04, 1.0);
                    this->runAction(Sequence::create(scaleAction1, scaleAction2, nullptr));
                }
                _speed *= -0.07f;
            }
        }
    }
}
