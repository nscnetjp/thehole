#ifndef __NATIVE_CONTROLLER_H__
#define __NATIVE_CONTROLLER_H__

namespace platform
{
    
    class NativeBridge
    {
    public:
        static void executeNative();
        static void nscJumpNative();
        static void setBanner();
    };
    
}

#endif //__NATIVE_CONTROLLER_H__
