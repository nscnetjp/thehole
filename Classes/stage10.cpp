#include "stage10.h"

USING_NS_CC;
using namespace ui;

Scene* Stage10::createScene()
{
    auto scene = Scene::create();
    
    auto layer = Stage10::create();
    layer->setTag(PARENT_LAYER_TAG);
    scene->addChild(layer);
    
    return scene;
}

bool Stage10::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
    setCommon(STAGE_10);
    
    _stageLayer0 = Layer::create();
    _stageLayer->addChild(_stageLayer0,STAGE_LAYER_0);
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Point origin = Director::getInstance()->getVisibleOrigin();
    _hairPosition = Point(330, -325 + visibleSize.height + origin.y);
    _armDefaultPosition = Point(640, origin.y - 380);
    _armUsePosition = Point(260,  origin.y);
    _zoomCount = 0;
    _hairPositionY = 0.0;
    _glassStatus = ITEM_SHAKE;
    _zoomStatus = NORMAL;
    _wigStatus = ON_HEAD;
    
    Device::setAccelerometerEnabled(true);
    auto accelerationListener = EventListenerAcceleration::create(CC_CALLBACK_2(Stage10::onAcceleration, this));
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(accelerationListener, this);
    //タッチ
    auto dispatcher = Director::getInstance()->getEventDispatcher();
    auto listener = EventListenerTouchOneByOne::create();
    listener->onTouchBegan = CC_CALLBACK_2(Stage10::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(Stage10::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(Stage10::onTouchEnded, this);
    dispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    
    // _stageLayer0 にのせていく
    //背景
    auto roomSprite = Sprite::create("Graphics/10/room10.png");
    roomSprite->setAnchorPoint(Point::ZERO);
    roomSprite->setPosition(Point::ZERO);
    _stageLayer0->addChild(roomSprite, ROOM);
    //おじさん
    _oji = Sprite::create("Graphics/10/oji10.png");
    _oji->setPosition(Point(320, 450));
    _stageLayer0->addChild(_oji, OJISAN);
    //虫眼鏡
    _glass = Sprite::create("Graphics/10/glass10_0.png");
    _glass->setPosition(Point(500, 1140));
    _stageLayer0->addChild(_glass, SWORD);
    
    // _stageLayer にのせていく
    //拡大したときの画像
    _zoomImg = Sprite::create("Graphics/10/oji10_1.png");
    _zoomImg->setAnchorPoint(Point::ZERO);
    _zoomImg->setPosition(Point::ZERO);
    _stageLayer->addChild(_zoomImg, ZOOM_IMG);
    _zoomImg->setVisible(false);
    //拡大したときの背景画像
    _ojiZoom = Sprite::create("Graphics/10/oji10_1.png");
    _ojiZoom->setAnchorPoint(Point::ZERO);
    _ojiZoom->setPosition(Point::ZERO);
    _stageLayer->addChild(_ojiZoom, ZOOM_IMG);
    _ojiZoom->setVisible(false);
    //１段階拡大したときのおじさん画像
    _ojiZoom0 = Sprite::create("Graphics/10/oji10_1_1.png");
    _ojiZoom0->setAnchorPoint(Point::ANCHOR_MIDDLE_TOP);
    _ojiZoom0->setPosition(Point(320, -200 + visibleSize.height + origin.y));
    _stageLayer->addChild(_ojiZoom0, OJISAN_ZOOM_0);
    _ojiZoom0->setVisible(false);
    //１段階拡大したときのおじさんの髪の毛画像後ろ
    _ojiZoom0Secret = Sprite::create("Graphics/10/secret10_1.png");
    _ojiZoom0Secret->setPosition(_hairPosition);
    _stageLayer->addChild(_ojiZoom0Secret, HAIR_SECRET);
    _ojiZoom0Secret->setVisible(false);
    //１段階拡大したときのおじさんの髪の毛画像前
    _ojiZoom0Hair = Sprite::create("Graphics/10/secret10_0.png");
    _ojiZoom0Hair->setPosition(_hairPosition);
    _stageLayer->addChild(_ojiZoom0Hair, HAIR);
    _ojiZoom0Hair->setVisible(false);
    //入手アイテム画像
    _item = Sprite::create("Graphics/10/glass10_off.png");
    _item->setPosition(Point(_getItemPosition.x,_getItemPosition.y - 200));
    _stageLayer->addChild(_item, ITEM_BTN);
    _item->setVisible(false);
    //入手アイテム画像
    _arm = Sprite::create("Graphics/10/arm10.png");
    _arm->setAnchorPoint(Point::ZERO);
    _arm->setPosition(_armDefaultPosition);
    _stageLayer->addChild(_arm, ARM);
    //アイテムボタン
    _itemButton = Button::create();
    _itemButton->setTouchEnabled(false);
    _itemButton->loadTextures("Graphics/common/item_btn.png", "Graphics/common/item_btn.png", "");
    _itemButton->setPosition(_getItemPosition);
    _itemButton->addTouchEventListener(CC_CALLBACK_2(Stage10::touchEvent, this));
    _stageLayer->addChild(_itemButton, ITEM_BTN, ITEM_ACT);
    //クリアボタン
    _clearButton = Button::create();
    _clearButton->setTouchEnabled(false);
    _clearButton->loadTextures("Graphics/common/clear_btn.png", "Graphics/common/clear_btn.png", "");
    _clearButton->setPosition(Point(220, 620));
    _clearButton->addTouchEventListener(CC_CALLBACK_2(StageBaseScene::touchEvent, this));
    _stageLayer->addChild(_clearButton, CLEAR_BTN, STAGE_CLEAR_ACT);
    //シークレットボタン
    _secretButton = Button::create();
    _secretButton->setTouchEnabled(false);
    _secretButton->loadTextures("Graphics/common/secret_btn.png", "Graphics/common/secret_btn.png", "");
    _secretButton->setAnchorPoint(Point::ANCHOR_BOTTOM_RIGHT);
    _secretButton->setPosition(_hairPosition);
    _secretButton->addTouchEventListener(CC_CALLBACK_2(Stage10::touchEvent, this));
    _stageLayer->addChild(_secretButton, SECRET_BTN, SECRET_ACT);

    
    return true;
}

void Stage10::onAcceleration(Acceleration *acc, Event *unused_event)
{
    if (_glassStatus == ITEM_SHAKE)
    {
        if (acc->x < -1.15)
        {
            _glassStatus = ITEM_FALL;
            ActionInterval* moveAct = EaseIn::create(MoveBy::create(0.8, Point(0, -830)), 2);
            cocos2d::CallFunc *callback = CallFunc::create([this]()
                                                           {
                                                               glassFall();
                                                           });
            _glass->runAction(Sequence::create(moveAct, callback, nullptr));
        }
    }
}

void Stage10::glassFall()
{
    _glassStatus = ITEM_FELL;
    _glass->setTexture("Graphics/10/glass10_1.png");
}

bool Stage10::onTouchBegan(Touch *touch, Event *event)
{
    //虫眼鏡もった腕つかむ
    if (_glassStatus == ITEM_USE)
    {
        Point touchPoint = touch->getLocation();
        Rect armRect = Rect(_arm->getPosition().x + 187, _arm  ->getPosition().y + 126, 150, 150);
        
        if(armRect.containsPoint(touchPoint))
        {
            _glassStatus = ITEM_USE_MOVE;
            _armTouchPoint = _arm->getPosition() - touch->getLocation();
        }
    }
    
    //虫眼鏡拾うとき
    if (_zoomStatus == NORMAL)
    {
        if (_glassStatus == ITEM_FELL)
        {
            Point touchPoint = touch->getLocation();
            Rect glassRect = _glass->boundingBox();
            
            if(glassRect.containsPoint(touchPoint))
            {
                _zoomStatus = ZOOM_0;
                _glassStatus = ITEM_UNUSE;
                _glass->removeFromParentAndCleanup(true);
                
                _item->setVisible(true);
                _item->runAction(EaseOut::create(JumpTo::create(0.2, _getItemPosition, 100, 1), 2));
                _itemButton->setTouchEnabled(true);
                
                //アイテム使用時のズーム判定のためのもの
                _zoomRect = Rect(257, 603, 100, 100);
                scheduleUpdate();
            }
        }
    }
    //カツラつかむとき
    else if (_zoomStatus == ZOOM_1)
    {
        Point touchPoint = touch->getLocation();
        Rect hairRect = Rect(_ojiZoom0Hair->getPosition().x - 200, _ojiZoom0Hair->getPosition().y, 400, 150);
        
        if(hairRect.containsPoint(touchPoint))
        {
            _wigStatus = CATCH_WIG;
            _hairPositionY = _ojiZoom0Hair->getPosition().y - touchPoint.y;
        }
    }
    
    return true;
}

void Stage10::onTouchMoved(Touch *touch, Event *event)
{
    //虫眼鏡もった腕動かす
    if (_glassStatus == ITEM_USE_MOVE)
    {
        Point armPosition = touch->getLocation() + _armTouchPoint;
        armPosition.x = MAX(-110, MIN(armPosition.x, 400));
        armPosition.y = MAX(-150, MIN(armPosition.y, 710));
        _arm->setPosition(armPosition);
        
        _zoomPoint = armPosition + Point(113, 345);
        log("zoomPoint x:%f y:%f",_zoomPoint.x, _zoomPoint.y);
    }
    //カツラ動かす
    if (_wigStatus == CATCH_WIG)
    {
        float hairY = MAX(_hairPosition.y, MIN(touch->getLocation().y + _hairPositionY, _hairPosition.y + 200));
        _ojiZoom0Hair->setPosition(Point(_hairPosition.x, hairY));
        _ojiZoom0Secret->setPosition(_ojiZoom0Hair->getPosition());
        _secretButton->setPosition(_ojiZoom0Hair->getPosition());
    }
}

void Stage10::onTouchEnded(cocos2d::Touch* touch, cocos2d::Event* event)
{
    //虫眼鏡もった腕について
    if (_glassStatus == ITEM_USE_MOVE)
    {
        _glassStatus = ITEM_RETURN;
        ActionInterval* moveAct = EaseInOut::create(MoveTo::create(0.3f, _armUsePosition), 2);
        cocos2d::CallFunc *callback = CallFunc::create([this]()
                                                       {
                                                           itemRetrun();
                                                       });
        _arm->runAction(Sequence::create(moveAct, callback, nullptr));
    }
    //カツラについて
    if (_zoomStatus == ZOOM_1)
    {
        float ojiHairY = _ojiZoom0Hair->getPosition().y;
        
        if (ojiHairY <= _hairPosition.y)
        {
            _wigStatus = ON_HEAD;
            _secretButton->setTouchEnabled(false);
        }
        else if (ojiHairY < _hairPosition.y + 100)
        {
            _wigStatus = OVER_HEAD;
            _secretButton->setTouchEnabled(false);
        }
        else if (ojiHairY >= _hairPosition.y + 100)
        {
            _wigStatus = OVER_HEAD;
            _secretButton->setTouchEnabled(true);
        }
    }
}

void Stage10::update(float dt)
{
    if (_glassStatus == ITEM_USE_MOVE)
    {
        //虫眼鏡中心とズームする場所が重なっていたら
        if(_zoomRect.containsPoint(_zoomPoint))
        {
            _zoomCount++;
            
            if (_zoomCount > 20) //一定時間重なっていたらズーム
            {
                ActionInterval* scaleAct = EaseIn::create(ScaleBy::create(0.8, 1.8), 3);
                cocos2d::CallFunc *callback = CallFunc::create([this]()
                                                               {
                                                                   ojiZoom();
                                                               });
                _stageLayer->runAction(Sequence::create(scaleAct, callback, nullptr));
                _itemButton->setTouchEnabled(false);
                _glassStatus = ITEM_RETURN;
                _zoomCount = 0;
            }
        }
        else
        {
            _zoomCount = 0;
        }
    }
}

void Stage10::itemRetrun()
{
    _glassStatus = ITEM_USE;
}

void Stage10::touchEvent(Ref* pSender, cocos2d::ui::Widget::TouchEventType type)
{
    Node *node = dynamic_cast<Node *>(pSender);
    int buttonTag = node->getTag();
    //入手アイテムオンオフボタン
    if (buttonTag == ITEM_ACT)
    {
        switch (type)
        {
            case Widget::TouchEventType::BEGAN:
                if (_glassStatus == ITEM_UNUSE) //使っていない場合
                {
                    //使っている画像に
                    _item->setTexture("Graphics/10/glass10_on.png");
                    
                    _arm->stopAllActions();
                    _arm->runAction(EaseOut::create(MoveTo::create(0.2f, _armUsePosition), 2));
                }
                break;
                
            case Widget::TouchEventType::ENDED:
                if (_glassStatus == ITEM_USE) //使っている場合
                {
                    //使っていない状態に
                    _glassStatus = ITEM_UNUSE;
                    _item->setTexture("Graphics/10/glass10_off.png");
                    
                    _arm->stopAllActions();
                    _arm->runAction(EaseIn::create(MoveTo::create(0.2f, _armDefaultPosition), 2));
                    if (_zoomStatus == ZOOM_3)
                    {
                        _clearButton->setTouchEnabled(false);
                    }
                }
                else if (_glassStatus == ITEM_UNUSE)
                {
                    _glassStatus = ITEM_USE;
                }
                break;
                
            case Widget::TouchEventType::CANCELED:
                if (_glassStatus == ITEM_UNUSE)
                {
                    _glassStatus = ITEM_USE;
                }
                break;
                
            default:
                break;
        }
    }
    //シークレット
    else if (buttonTag == SECRET_ACT &&
             _zoomStatus == ZOOM_1)
    {
        if (type == Widget::TouchEventType::ENDED)
        {
            _secretButton->setTouchEnabled(false);
            _ojiZoom0Secret->setTexture("Graphics/10/secret10_2.png");
            getSecret();
        }
    }
    
}

void Stage10::ojiZoom()
{
    _stageLayer->setScale(1.0);
    
    _glassStatus = ITEM_UNUSE;
    _item->setTexture("Graphics/10/glass10_off.png");
    
    _arm->setPosition(Point(640, - _arm->getContentSize().height));
    _itemButton->setTouchEnabled(true);
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Point origin = Director::getInstance()->getVisibleOrigin();
    
    switch (_zoomStatus)
    {
        case ZOOM_0:
            _zoomStatus = ZOOM_1;
            _stageLayer0->removeFromParentAndCleanup(true);
            _ojiZoom->setVisible(true);
            _ojiZoom0->setVisible(true);
            _ojiZoom0Secret->setVisible(true);
            _ojiZoom0Hair->setVisible(true);
            _zoomImg->setVisible(true);
            _zoomRect = Rect(232, -640 + visibleSize.height + origin.y, 170, 80);
            break;
            
        case ZOOM_1:
            _zoomStatus = ZOOM_2;
            _ojiZoom->removeFromParentAndCleanup(true);
            _ojiZoom0->removeFromParentAndCleanup(true);
            _ojiZoom0Secret->removeFromParentAndCleanup(true);
            _ojiZoom0Hair->removeFromParentAndCleanup(true);
            _zoomImg->setTexture("Graphics/10/oji10_2.png");
            _zoomRect = Rect(130, 515, 360, 150);
            break;
            
        case ZOOM_2:
            _zoomStatus = ZOOM_3;
            _zoomImg->setTexture("Graphics/10/oji10_3_hole.png");
            
            _item->removeFromParentAndCleanup(true);
            _itemButton->removeFromParentAndCleanup(true);
            _clearButton->setTouchEnabled(true);
            break;
            
        default:
            break;
    }
}
