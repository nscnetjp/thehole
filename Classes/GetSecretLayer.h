#ifndef __MyCppGame__GetSecretLayer__
#define __MyCppGame__GetSecretLayer__

#include "cocos2d.h"
#include "ReplaceStage.h"
#include <ui/UIButton.h>

class GetSecretLayer : public cocos2d::LayerColor
{
public:
    virtual bool init();
    CREATE_FUNC(GetSecretLayer);
    
    void saveStageNo(STAGE stage);
    
private:
    enum SPRITE_LAP
    {
        SECRET = 0,
        GET,
        CLOSE
    };
    void touchEvent(Ref* pSender, cocos2d::ui::Widget::TouchEventType type);
    
    void startAction();
    void makeCloseBtn();
    void endGetAction();
    
    cocos2d::Sprite* _secretSprite;
    cocos2d::Sprite* _getSprite;
    cocos2d::ui::Button* _closeButton;
    
    STAGE _secretNo;
};

#endif /* defined(__MyCppGame__GetSecretLayer__) */
