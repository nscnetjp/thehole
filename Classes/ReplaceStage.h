#ifndef __REPLACE_STAGE_H__
#define __REPLACE_STAGE_H__

#include "cocos2d.h"

enum STAGE
{
    STAGE_01 = 0,
    STAGE_02,
    STAGE_03,
    STAGE_04,
    STAGE_05,
    STAGE_06,
    STAGE_07,
    STAGE_08,
    STAGE_09,
    STAGE_10,
    GAME_CLEAR
};

class ReplaceStageClass
{
public:
    static void replaceStage(int stageNo);
};

#endif /* defined(__REPLACE_STAGE_H__) */
