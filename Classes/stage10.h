#ifndef __STAGE_10_H__
#define __STAGE_10_H__

#include "cocos2d.h"
#include "StageBaseScene.h"

class Stage10 : public StageBaseScene
{
public:
    static cocos2d::Scene* createScene();
    virtual bool init();
    CREATE_FUNC(Stage10);
private:
    enum LAYER_LAP
    {
        ROOM = 0,
        OJISAN,
        BARREL,
        SWORD,
        GLASS
    };
    enum ZOOM_LAYER_LAP
    {
        STAGE_LAYER_0 = 0,
        ZOOM_IMG,
        HAIR_SECRET,
        OJISAN_ZOOM_0,
        HAIR,
        ARM,
        ITEM_BTN,
        ZOOM_BTN,
        CLEAR_BTN,
        SECRET_BTN
    };
    enum BUTTON_TAG
    {
        ITEM_ACT = 0,
        SECRET_ACT
    };
    enum GLASS_STATUS
    {
        ITEM_SHAKE = 0,
        ITEM_FALL,
        ITEM_FELL,
        ITEM_UNUSE,
        ITEM_USE,
        ITEM_USE_MOVE,
        ITEM_RETURN
    };
    enum ZOOM_STATUS
    {
        NORMAL = 0,
        ZOOM_0,
        ZOOM_1,
        ZOOM_2,
        ZOOM_3
    };
    enum WIG_STATUS
    {
        ON_HEAD = 0,
        CATCH_WIG,
        OVER_HEAD
    };
    void onAcceleration(cocos2d::Acceleration* acc, cocos2d::Event* unused_event);
    bool onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* event);
    void onTouchMoved(cocos2d::Touch* touch, cocos2d::Event* event);
    void onTouchEnded(cocos2d::Touch* touch, cocos2d::Event* event);
    void touchEvent(Ref* pSender, cocos2d::ui::Widget::TouchEventType type);
    virtual void update(float dt);
    
    void glassFall();
    void ojiZoom();
    void itemRetrun();
    
    cocos2d::Layer* _stageLayer0;
    cocos2d::Sprite* _oji;
    cocos2d::Sprite* _ojiZoom;
    cocos2d::Sprite* _ojiZoom0;
    cocos2d::Sprite* _ojiZoom0Secret;
    cocos2d::Sprite* _ojiZoom0Hair;
    cocos2d::Sprite* _item;
    cocos2d::Sprite* _glass;
    cocos2d::Sprite* _arm;
    cocos2d::Sprite* _zoomImg;
    cocos2d::ui::Button* _clearButton;
    cocos2d::ui::Button* _secretButton;
    cocos2d::ui::Button* _itemButton;
    cocos2d::ui::Button* _glassdButton;
    
    cocos2d::Rect _zoomRect;
    cocos2d::Point _zoomPoint;
    cocos2d::Point _touchBeganPoint;
    cocos2d::Point _armDefaultPosition;
    cocos2d::Point _armUsePosition;
    cocos2d::Point _armTouchPoint;
    cocos2d::Point _hairPosition;
    int _zoomCount;
    float _hairPositionY;
    GLASS_STATUS _glassStatus;
    ZOOM_STATUS _zoomStatus;
    WIG_STATUS _wigStatus;
};

#endif /* defined(__STAGE_10_H__) */
