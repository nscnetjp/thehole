#ifndef __STAGE_06_H__
#define __STAGE_06_H__

#include "cocos2d.h"
#include "StageBaseScene.h"

class Stage06 : public StageBaseScene
{
public:
    static cocos2d::Scene* createScene();
    virtual bool init();
    CREATE_FUNC(Stage06);
private:
    enum LAYER_LAP
    {
        ROOM = 0,
        PAINT,
        OJISAN,
        ROOM_FRONT,
        BAR,
        CLEAR_BTN,
        SECRET_BTN
    };
    enum PAINT_STATUS
    {
        STOP = 0,
        RUN,
        END,
    };
    enum OJI_STATUS
    {
        HUNG = 0,
        OJI_TOUCH_BEGAN,
        OJI_TOUCH_MOVE,
        FALLING,
        FELL,
        RETURN,
        LOSE_SECRET
    };
    
    bool onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* event);
    void onTouchMoved(cocos2d::Touch* touch, cocos2d::Event* event);
    void onTouchEnded(cocos2d::Touch* touch, cocos2d::Event* event);
    void touchEvent(Ref* pSender, cocos2d::ui::Widget::TouchEventType type);
    void onAcceleration(cocos2d::Acceleration* acc, cocos2d::Event* unused_event);
    virtual void update(float dt);
    
    void makeHole(float tm);
    void ojiFall();
    void ojiFell();
    void holeInEnabled();
    void afterGetSecret();
    
    cocos2d::Sprite* _paint0;
    cocos2d::Sprite* _paint1;
    cocos2d::Sprite* _oji;
    cocos2d::Sprite* _roomBack;
    cocos2d::Sprite* _roomFront;
    cocos2d::ui::Button* _clearButton;
    cocos2d::ui::Button* _secretButton;
    
    cocos2d::Point _touchBeganPoint;
    cocos2d::Point _ojiPosition;
    float _touchMoveTime;
    float _accX;
    float _ojiSpeedX;
    PAINT_STATUS _paintStatus;
    OJI_STATUS _ojiStatus;
};

#endif /* defined(__STAGE_06_H__) */
