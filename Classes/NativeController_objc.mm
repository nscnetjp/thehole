#import "NativeController_objc.h"
#import "platform/CCDevice.h"

@implementation NativeController_objc

+ (void)executeObjc
{
    //何らかの処理
}

+ (void)nscJumpObjc
{
    NSString *urlString = @"http://www.google.co.jp"; //TODO:NSCのURLに
    NSURL *url = [NSURL URLWithString:urlString];
    
    // ブラウザを起動する
    [[UIApplication sharedApplication] openURL:url];
}

+ (void)setBannerObjc
{
    //バナー広告表示
}



@end
