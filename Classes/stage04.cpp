#include "stage04.h"

USING_NS_CC;
using namespace ui;

static const Point BALL_POSITION = Point(530, 368);
static const float BALL_MAX_SPEED = 800;
static const float ROOM_AREA = 200;

Scene* Stage04::createScene()
{
    auto scene = Scene::create();
    
    auto layer = Stage04::create();
    layer->setTag(PARENT_LAYER_TAG);
    scene->addChild(layer);
    
    return scene;
}

bool Stage04::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
    setCommon(STAGE_04);
    
    _manholePosition = Point(320, 480);
    _ballPosition = BALL_POSITION;
    _accX = 0.0;
    _ballSpeedX = 0.0;
    _manholeStatus = NOMAL;
    _btnStatus = SECRET_HIDDEN;
    
    Device::setAccelerometerEnabled(true);
    auto accelerationListener = EventListenerAcceleration::create(CC_CALLBACK_2(Stage04::onAcceleration, this));
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(accelerationListener, this);
    
    //背景
    auto roomSprite = Sprite::create("Graphics/04/room04.png");
    roomSprite->setAnchorPoint(Point::ZERO);
    roomSprite->setPosition(Point::ZERO);
    _stageLayer->addChild(roomSprite, ROOM);
    //マンホール
    _manhole = Sprite::create("Graphics/04/manhole04.png");
    _manhole->setAnchorPoint(Point::ANCHOR_MIDDLE_BOTTOM);
    _manhole->setPosition(_manholePosition);
    _stageLayer->addChild(_manhole, MANHOLE);
    //TODO:サッカーボールの陰
    _ballShadow = Sprite::create("Graphics/04/ball04_shade.png");
    _ballShadow->setPosition(_ballPosition);
    _stageLayer->addChild(_ballShadow, BALL_SHADOW);
    //サッカーボール
    _ball = Sprite::create("Graphics/04/ball04_0.png");
    _ball->setPosition(_ballPosition);
    _stageLayer->addChild(_ball, BALL);
    //クリアボタン
    _clearButton = Button::create();
    _clearButton->setTouchEnabled(false);
    _clearButton->loadTextures("Graphics/common/clear_btn.png", "Graphics/common/clear_btn.png", "");
    _clearButton->setPosition(Point(320, 580));
    _clearButton->addTouchEventListener(CC_CALLBACK_2(StageBaseScene::touchEvent, this));
    _stageLayer->addChild(_clearButton, CLEAR_BTN, STAGE_CLEAR_ACT);
    //シークレット画像
    _secret = Sprite::create("Graphics/04/secret04_close.png");
    _secret->setPosition(Point(110, 322));
    _stageLayer->addChild(_secret, SECRET);
    //シークレットボタン
    Button* secretButton = Button::create();
    secretButton->setTouchEnabled(true);
    secretButton->loadTextures("Graphics/common/secret_btn.png", "Graphics/common/secret_btn.png", "");
    secretButton->setPosition(Point(110, 290));
    secretButton->addTouchEventListener(CC_CALLBACK_2(Stage04::touchEvent, this));
    _stageLayer->addChild(secretButton, CLEAR_BTN, 10000);
    
    scheduleUpdate();
    
    return true;
}

void Stage04::onAcceleration(Acceleration *acc, Event *unused_event)
{
    if (acc->z > 0.6 && _manholeStatus == NOMAL)
    {
        _manholeStatus = FELL;
        manholeAct1();
    }
    
    
    _accX = acc->x;
    if (fabs(_accX) < 0.2)
    {
        _accX = 0.0;
    }
    
    log("%f",acc->x);
}

void Stage04::touchEvent(Ref *pSender, Widget::TouchEventType type)
{   
    if (type == Widget::TouchEventType::ENDED)
    {
        switch (_btnStatus)
        {
            case SECRET_HIDDEN:
                _btnStatus = SHOW_SECRET;
                _secret->setTexture("Graphics/04/secret04_open.png");
                break;
                
            case SHOW_SECRET:
                _btnStatus = GOT_SECRET_OPEN;
                _secret->setTexture("Graphics/04/secret04_open_got.png");
                getSecret();
                break;
                
            case GOT_SECRET_OPEN:
                _btnStatus = GOT_SECRET_CLOSE;
                _secret->setTexture("Graphics/04/secret04_close.png");
                break;
                
            case GOT_SECRET_CLOSE:
                _btnStatus = GOT_SECRET_OPEN;
                _secret->setTexture("Graphics/04/secret04_open_got.png");
                break;
                
            default:
                break;
        }
    }
}

void Stage04::update(float dt)
{
    //加速度の変数を使ってボールを転がす
    _ballSpeedX += _accX * 100;
    _ballSpeedX *= 0.95;
    _ballSpeedX = MAX(-BALL_MAX_SPEED, MIN(_ballSpeedX, BALL_MAX_SPEED));
    _ballPosition.x += _ballSpeedX * dt;
    _ballPosition.x = MAX(-ROOM_AREA, MIN(_ballPosition.x, ROOM_AREA + 640));
    _ballShadow->setPosition(_ballPosition);
    _ball->setPosition(_ballPosition);
    _ball->setRotation((_ballPosition.x - BALL_POSITION.x) * (360 / 280));
}

void Stage04::manholeAct1()
{
    //マンホールが手前に来るような演出
    _manhole->setScale(1.05);
    ActionInterval* scaleAct = EaseIn::create(ScaleBy::create(0.2, 1.1, 0.5), 2.5);
    cocos2d::CallFunc *callback = CallFunc::create([this]()
                                                    {
                                                        manholeAct2();
                                                    });
    _manhole->runAction(Sequence::create(scaleAct, callback, nullptr));
}

void Stage04::manholeAct2()
{
    _clearButton->setTouchEnabled(true);
    //マンホールの画像が切り替わってからの落ちる動き
    _manhole->setScale(1.0);
    _manhole->setTexture("Graphics/04/manhole04_fall.png");
    _manhole->setPosition(Point(_manholePosition.x, _manholePosition.y -100));
    _manhole->runAction(MoveBy::create(0.1, Point(0, - 150)));
}
