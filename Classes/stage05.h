#ifndef __STAGE_05_H__
#define __STAGE_05_H__

#include "cocos2d.h"
#include "StageBaseScene.h"

class Stage05 : public StageBaseScene
{
public:
    static cocos2d::Scene* createScene();
    virtual bool init();
    CREATE_FUNC(Stage05);
    
private:
    enum LAYER_LAP
    {
        SECTION = 0,
        CHIN_WALL,
        ROOM,
        EYE_WALL,
        SECRET,
        CLEAR_BTN,
        SECRET_BTN
    };
    enum MOUTH_STATUS
    {
        CLOSE = 0,
        BEGIN_OPEN,
        OPEN
    };
    enum SECRET_STATUS
    {
        SECRET_CLOSE_0 = 0,
        SECRET_OPEN,//片目開けるとき
        SECRET_CLOSE_1,
        SECRET_RESET,
        SECRET_END
    };
    
    bool onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* event);
    void onTouchMoved(cocos2d::Touch* touch, cocos2d::Event* event);
    void onTouchEnded(cocos2d::Touch* touch, cocos2d::Event* event);
    void touchEvent(Ref* pSender, cocos2d::ui::Widget::TouchEventType type);
    virtual void update(float dt);
    
    void holeInEnabled();
    void secretBtnEnabled();
    
    cocos2d::Sprite* _eyeWall;
    cocos2d::Sprite* _chinWall;
    cocos2d::Sprite* _secret;
    cocos2d::ui::Button* _clearButton;
    cocos2d::ui::Button* _secretButton;
    
    float _secretTime;
    cocos2d::Point _touchBeganPoint;
    MOUTH_STATUS _mouthStatus;
    SECRET_STATUS _secretStatus;
};

#endif /* defined(__STAGE_05_H__) */
