#include "StageSelectLayer.h"

USING_NS_CC;

bool StageSelectLayer::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
//    std::string bgImgName[3] =
//    {
//        "select_1.png",
//        "select_2.png",
//        "select_3.png"
//    };
    
    for (int i = 0; i < 6; i++)
    {
        std::string holeImgName = StringUtils::format("Graphics/top/select_%d.png", i);
        long len = holeImgName.length();
        char* fname = new char[len+1];
        memcpy(fname, holeImgName.c_str(), len+1);
        
        auto bgSprite = Sprite::create(fname);
        bgSprite->setAnchorPoint(Point::ZERO);
        bgSprite->setPosition(Point(500 * i, 0));
        
        this->addChild(bgSprite);
    }
    
    return true;
}