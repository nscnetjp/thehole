#include "stage09.h"

USING_NS_CC;
using namespace ui;

static const Point SHUTTER_POSITION = Point(320, 955);
static const Point SECRET_POSITION = Point(169, 1000);
static const float OJI_ACTION_DURATION = 0.1;
static const float PI = 3.141592;
static const float ROTATE_MAX_SPEED = 300.0;

Scene* Stage09::createScene()
{
    auto scene = Scene::create();
    
    auto layer = Stage09::create();
    layer->setTag(PARENT_LAYER_TAG);
    scene->addChild(layer);
    
    return scene;
}

bool Stage09::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
    setCommon(STAGE_09);
    
    _coverStatus = NORMAL;
    _ojiStatus = HIDDEN;
    _shutterBtnStatus = HIDDEN_BTN;
    _shutterStatus = SHUTTER_OPEN;
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Point origin = Director::getInstance()->getVisibleOrigin();
    
    //加速度センサー
    Device::setAccelerometerEnabled(true);
    auto accelerationListener = EventListenerAcceleration::create(CC_CALLBACK_2(Stage09::onAcceleration, this));
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(accelerationListener, this);
    //タッチ
    auto dispatcher = Director::getInstance()->getEventDispatcher();
    auto listener = EventListenerTouchOneByOne::create();
    listener->onTouchBegan = CC_CALLBACK_2(Stage09::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(Stage09::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(Stage09::onTouchEnded, this);
    dispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    
    auto roomSprite = Sprite::create("Graphics/09/room09.png");
    roomSprite->setAnchorPoint(Point::ZERO);
    roomSprite->setPosition(Point::ZERO);
    _stageLayer->addChild(roomSprite, ROOM);
    //排気口
    _exhaustPort = Sprite::create("Graphics/common/exhaustPort.png");
    _exhaustPort->setPosition(Point(590, -140 + visibleSize.height + origin.y));
    _stageLayer->addChild(_exhaustPort, EXHAUST_PORT);
    //画面右下穴を覆っているもの
    _cover = Sprite::create("Graphics/09/cover09.png");
    _cover->setPosition(Point(510, 260));
    _stageLayer->addChild(_cover, COVER);
    //クリアボタン
    _clearButton = Button::create();
    _clearButton->setTouchEnabled(false);
    _clearButton->loadTextures("Graphics/common/clear_btn.png", "Graphics/common/clear_btn.png", "");
    _clearButton->setPosition(Point(510, 260));
    _clearButton->addTouchEventListener(CC_CALLBACK_2(StageBaseScene::touchEvent, this));
    _stageLayer->addChild(_clearButton, CLEAR_BTN, STAGE_CLEAR_ACT);
    //おじさん
    _oji = Sprite::create("Graphics/09/oji09.png");
    _oji->setPosition(Point(320, 500));
    _stageLayer->addChild(_oji, OJISAN);
    //シャッターのスイッチ
    _shutterBtn = Sprite::create("Graphics/09/shutter09_switch.png");
    _shutterBtn->setAnchorPoint(Point::ANCHOR_MIDDLE_TOP);
    _shutterBtn->setPosition(Point(-30, 1150));
    _stageLayer->addChild(_shutterBtn, SHUTTER_BTN);
    _shutterBtn->setRotation(25);
    //シャッターボタン
    _shutterButton = Button::create();
    _shutterButton->setTouchEnabled(false);
    _shutterButton->loadTextures("Graphics/common/secret_btn.png", "Graphics/common/secret_btn.png", "");
    _shutterButton->setAnchorPoint(Point(0.5,7.1));
    _shutterButton->setPosition(_shutterBtn->getPosition());
    _shutterButton->addTouchEventListener(CC_CALLBACK_2(Stage09::touchEvent, this));
    _stageLayer->addChild(_shutterButton, SHUTTER_BTN, SHUTTER_ACT);
    _shutterButton->setRotation(_shutterBtn->getRotation());
    //シャッター
    _shutter = Sprite::create("Graphics/09/shutter09.png");
    _shutter->setPosition(SHUTTER_POSITION);
    _stageLayer->addChild(_shutter, SHUTTER);
    //シークレット
    _secret = Sprite::create("Graphics/09/secret09.png");
    _secret->setPosition(SECRET_POSITION);
    _stageLayer->addChild(_secret, SECRET);
    //シークレットボタン
    _secretButton = Button::create();
    _secretButton->setTouchEnabled(false);
    _secretButton->loadTextures("Graphics/common/secret_btn.png", "Graphics/common/secret_btn.png", "");
    _secretButton->setPosition(Point(169, 760));
    _secretButton->addTouchEventListener(CC_CALLBACK_2(Stage09::touchEvent, this));
    _stageLayer->addChild(_secretButton, SECRET_BTN, SECRET_ACT);
    
    return true;
}

void Stage09::onAcceleration(Acceleration *acc, Event *unused_event)
{
    if (_shutterBtnStatus == HIDDEN_BTN)
    {
        if (acc->x > 0.95)
        {
            _shutterBtnStatus = UNPUSH;
            _shutterButton->setTouchEnabled(true);
            scheduleUpdate();
        }
    }
    else if (_shutterBtnStatus == UNPUSH)
    {
        _accX = - acc->x;
        _accY = - acc->y;
        _accZ = acc->z;
    }
}

void Stage09::update(float dt)
{
    if (_shutterBtnStatus == UNPUSH)
    {
        //加速度より端末の下方向を検出
        if (fabs(_accZ) > 0.9)
        {
            _goalAngle = 10.0;
        }
        else if (fabs(_accZ) < 0.7)
        {
            float goalAngle = (atan2f(_accX, _accY) * 180 / PI);
            
            if (goalAngle < 30 && goalAngle > -140)
            {
                _goalAngle = goalAngle * 0.85;
                _goalAngle = MIN(30, MAX(_goalAngle, -95));
            }
        }
        //スイッチと下方向の角度で回転する加速度を決める
        float accBtnRotate = _goalAngle - _shutterBtn->getRotation();
        
        _btnRotateSpeed += accBtnRotate * 1.5;
        _btnRotateSpeed *= 0.9;
        _btnRotateSpeed = MAX(-ROTATE_MAX_SPEED, MIN(_btnRotateSpeed, ROTATE_MAX_SPEED));
        float _shutterBtnAngle = _shutterBtn->getRotation() + _btnRotateSpeed * dt;
        
        _shutterBtn->setRotation(_shutterBtnAngle);
        _shutterButton->setRotation(_shutterBtnAngle);
    }
    else if (_shutterBtnStatus == PUSH &&
             _shutterStatus != SHUTTER_CLOSE)
    {
        if (_shutterMoveY < 230)
        {
            _shutterMoveY += 1;
            
            if (_shutterMoveY > 200 &&
                _shutterStatus == SHUTTER_OPEN)
            {
                _shutterStatus = APPEAR_SECRET;
                _secretButton->setTouchEnabled(true);
                
                _ojiStatus = END;
            }
        }
        else
        {
            _shutterMoveY = 230;
            _shutterStatus = SHUTTER_CLOSE;
        }
        
        _shutter->setPosition(Point(SHUTTER_POSITION.x, SHUTTER_POSITION.y - _shutterMoveY));
        _secret->setPosition(Point(SECRET_POSITION.x, SECRET_POSITION.y - _shutterMoveY));
    }
}

bool Stage09::onTouchBegan(Touch *touch, Event *event)
{
    Point point = touch->getLocation();
    
    if (_coverStatus == NORMAL)
    {
        Rect  coverRect = _cover->boundingBox();
        
        if (coverRect.containsPoint(point))
        {
            ActionInterval* moveAct = MoveTo::create(3.0, Point(510, 190));
            cocos2d::CallFunc *callback = CallFunc::create([this]()
                                                           {
                                                               touchCover();
                                                           });
            _cover->runAction(Sequence::create(moveAct, callback, nullptr));
        }
    }
    
    if (_ojiStatus != END)
    {
        Rect wallHoleL = Rect(94, 676, 150, 150);
        Rect wallHoleR = Rect(398, 676, 150, 150);
        
        if (wallHoleL.containsPoint(point) &&
            _ojiStatus != LEFT)
        {
            _ojiStatus = LEFT;
            ActionInterval* rotateAct = RotateTo::create(OJI_ACTION_DURATION, -20);
            ActionInterval* moveAct = MoveTo::create(OJI_ACTION_DURATION, Point(180, 715));
            _oji->runAction(Spawn::create(rotateAct, moveAct, nullptr));
        }
        else if (wallHoleR.containsPoint(point) &&
                 _ojiStatus != RIGHT)
        {
            _ojiStatus = RIGHT;
            ActionInterval* rotateAct = RotateTo::create(OJI_ACTION_DURATION, 20);
            ActionInterval* moveAct = MoveTo::create(OJI_ACTION_DURATION, Point(460, 715));
            _oji->runAction(Spawn::create(rotateAct, moveAct, nullptr));
        }
    }
    
    return true;
}

void Stage09::touchEvent(Ref *pSender, Widget::TouchEventType type)
{
    Node *node = dynamic_cast<Node *>(pSender);
    int buttonTag = node->getTag();
    
    if (buttonTag == SHUTTER_ACT)
    {
        if (type == Widget::TouchEventType::BEGAN)
        {
            _shutterBtnStatus = PUSH;
            _btnRotateSpeed = 0.0;
        }
        else if (type == Widget::TouchEventType::ENDED ||
                 type == Widget::TouchEventType::CANCELED)
        {
            _shutterBtnStatus = UNPUSH;
        }
        
    }
    else if (buttonTag == SECRET_ACT)
    {
        if (type == Widget::TouchEventType::ENDED)
        {
            _secret->removeFromParentAndCleanup(true);
            _secretButton->removeFromParentAndCleanup(true);
            getSecret();
        }
    }
}

void Stage09::touchCover()
{
    _clearButton->setTouchEnabled(true);
}
